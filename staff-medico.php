<?php
    include 'src/includes/header.php'
?>
    <main>
        <section id="parallax" class="sct-banner scroll">
            <div class="degrade-int"></div>
            <img class="img-banner" src="/assets/images/banner/staffMedico.jpg" alt="">
            <div class="content-title-banner container">
                <h1 class="titleBanner text-uppercase">staff médico</h1>
            </div>
        </section>

        <!--SECCION STAFF MÉDICO-->
        <section class="wrapper-info-theClinic pb5 pt3 bg-white">
            <div class="container">
                <?php
                    include 'src/includes/filtro.php'
                ?>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 contentTitlesLetter">
                        <div class="row">
                            <div class="col-xs-12 col-lg-5 pd-x-0">
                                <h2 class="titles-descrip titles-descripCustomMovil"><span class="span-titlesDescrip">NUESTROS</span><br>ESPECIALISTAS</h2>
                            </div>
                            <div class="tabs text-uppercase">
                                <span class="p-internas tab-link current" data-tab="a">A</span>
                                <span class="p-internas tab-link" data-tab="b">B</span>
                                <span class="p-internas tab-link" data-tab="c">C</span>
                                <span class="p-internas tab-link" data-tab="d">D</span>
                                <span class="p-internas tab-link" data-tab="e">E</span>
                                <span class="p-internas tab-link" data-tab="f">F</span>
                                <span class="p-internas tab-link" data-tab="g">G</span>
                                <span class="p-internas tab-link" data-tab="h">H</span>
                                <span class="p-internas tab-link" data-tab="i">I</span>
                                <span class="p-internas tab-link" data-tab="j">J</span>
                                <span class="p-internas tab-link" data-tab="k">K</span>
                                <span class="p-internas tab-link" data-tab="l">L</span>
                                <span class="p-internas tab-link" data-tab="m">M</span>
                                <span class="p-internas tab-link" data-tab="n">N</span>
                                <span class="p-internas tab-link" data-tab="o">O</span>
                                <span class="p-internas tab-link" data-tab="p">P</span>
                                <span class="p-internas tab-link" data-tab="q">Q</span>
                                <span class="p-internas tab-link" data-tab="r">R</span>
                                <span class="p-internas tab-link" data-tab="s">S</span>
                                <span class="p-internas tab-link" data-tab="t">T</span>
                                <span class="p-internas tab-link" data-tab="u">U</span>
                                <span class="p-internas tab-link" data-tab="v">V</span>
                                <span class="p-internas tab-link" data-tab="w">W</span>
                                <span class="p-internas tab-link" data-tab="x">X</span>
                                <span class="p-internas tab-link" data-tab="y">Y</span>
                                <span class="p-internas tab-link" data-tab="z">Z</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 pd-x-0 pb5">
                        <div class="row">
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">Dr.  HUGO CASTELO JARA</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Traumatología</span>
                                                </div>
                                            </div>
                                        </figcaption>
                                        
                                        <a class="link-cardGrid lkcg" href="detalle-medico.php"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm2.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">Dr.  HECTOR MOLINA CALLE</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Cardiología</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm3.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">Dr. RICARDO BARRIGA DELGADO</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Urología</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm4.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">Dr.  HUGO CASTELO JARA</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Ortopedia</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm5.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">Dr.  CEVALLOS  LUIS  ÁNGEL</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Odontología</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm6.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">dr. cevallos luis ángel</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Odontología</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm7.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">dr. cevallos luis ángel</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Odontología</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                            <div class="card-staff col-xs-6 col-sm-4 col-lg-3">
                                <div class="grid-news grid-staff">
                                    <figure class="wrapper-news">
                                        <div class="content-img-staff">
                                            <img class="fondo-image" src="assets/images/internas/staff/sm8.jpg" alt="img18" />
                                        </div>
                                        <figcaption>
                                            <div class="title-news">
                                                <h2 class="nameDoctor">dr. cevallos luis ángel</h2>
                                                <div>
                                                    <h3 class="specialty">Especialidad:</h3>
                                                    <span class="p-internas spanSpecialty">Odontología</span>
                                                </div>
                                            </div>

                                        </figcaption>
                                        <a class="link-cardGrid lkcg" href="#"><div class="vm"></div></a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>

    <?php
            include 'src/includes/cierre.php'
        ?>