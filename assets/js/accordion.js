// CSS animation maps:
const rotateH = {
    transform: 'rotateZ(90deg)',
    left: 0
};
  
const rotateH_Rev = {
    transform: 'rotateZ(0deg)',
    top: '50%'
};
  
const rotateV = {
    transform: 'rotateZ(90deg)',
    top: 0
};

  
// Wait for DOM content to be loaded:
$(function () {
    $('.accordion li').on('click', '.inactive', evt => {
        const $currTarg = $(evt.currentTarget),
        $accordion_panel = $currTarg.next('.accordion .accordion-panel'),
        $lineH = $currTarg.find('.line-h');
        $('h4.active').trigger('click');
        $lineH.css(rotateH);
        setTimeout(() => {
        $lineH.parent().css({ transform: 'rotateZ(90deg)' });
        $currTarg.toggleClass('inactive active');
        }, 250);
    });

    $('.accordion li').on('click', '.active', evt => {
        const $currTarg = $(evt.currentTarget),
        $accordion_panel = $currTarg.next('.accordion .accordion-panel'),
        $lineH = $currTarg.find('.line-h');
        $currTarg.find('.accordion .glyph-wrapper').css({ transform: 'rotateZ(-90deg)' });
        setTimeout(() => {
        $lineH.css(rotateH_Rev);
        $currTarg.toggleClass('inactive active');
        }, 250);
    });

    $('.accordion .default').trigger('click');
});

$('.accordion .glyph-wrapper').on('click', function(){
    $('.accordion .glyph-wrapper.glyph-wrapper_color').removeClass('glyph-wrapper_color');
    $(this).addClass('glyph-wrapper_color');
});
