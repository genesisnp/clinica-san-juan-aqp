const btnModal1 = document.querySelectorAll('.span-pol');
const modal1 = document.querySelector('.modal-content1');
const modalCloseBtn1 = document.querySelector('#modal-close-btn');
const sctModal1 = document.querySelector('.sct-modal1');

btnModal1.forEach((btnModals) => {
    btnModals.addEventListener('click', (e) => {
        modal1.style.display = 'flex';
    })
})

modalCloseBtn1.addEventListener('click', (e) => {
    modal1.style.display = 'none';
});

modal1.addEventListener('click', (e) => {
    if(e.target.classList.contains('modal-container')) {
        modal1.style.display = 'none';
    }
});
sctModal1.addEventListener('click', (e)=> {
    modal1.style.display = 'none';
})

