<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner">
        <img class="img-banner" src="/assets/images/banner/vocacionHospitalaria.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">vocaciones hospitalarias</h1>
        </div>
    </section>
    <!--SECCION MENSAJEROS DE LA SALUD-->
    <section class="sct-hospital-vocation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 pd-x-0">
                    <h2 class="titles-descrip"><span class="icon-san"></span>
                        <span class="span-titlesDescrip">
                        ser hermano</span><br>de san juan dios</h2>
                </div>
                <div class="col-xs-12">
                    <div class="us-oh row">
                        <div class="img-us-oh float-right col-xs-12 col-md-10 pd-x-0">
                            <img class="img-cover" src="assets/images/internas/la-clinica/ser-hrno.jpg" alt="">
                        </div>
                        <div class="description-flotant descp-flot-l dscp-2">
                            <p class="text-border text-p2">La Orden Hospitalaria de San Juan de Dios es una Institución 
                                religiosa de confesionalidad católica y sin ánimo de lucro, que promueve la ayuda a 
                                necesitados y enfermos en los ámbitos social y sanitario. A través de la Hospitalidad 
                                fomentamos una asistencia integral y humanizada guiada por nuestros valores: calidad, 
                                respeto, responsabilidad y espiritualidad. </p>
                        </div>
                    </div>
                </div>

                <!-- PROCESO DE FORMACIÓN -->
                <div class="col-xs-12">
                    <h2 class="sub-ttl-flotant ttl-presence color-primary">Proceso de Formación<br>del Hermano de San Juan de Dios</h2>
                </div>
                <div class="col-xs-12 card-process-formation pd-x-0">
                    <div class="wrapper-card-repeat">
                        <div class="cols-value">
                            <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-conocimiento.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-aspirantado"></i>
                                            <p class="title-inner">aspirantado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-conocimiento.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">aspirantado</p>
                                            <p class="text-p2">Esta etapa inicial es un tiempo caracterizado por el mutuo conocimiento entre el 
                                                candidato y la Orden. Es un período de formación en el que el joven discierne si tiene cualidades 
                                                para ser un Hermano. Es un tiempo para profundizar en la propia experiencia humana y cristiana, 
                                                dedicado a conocerse, aceptarse, amarse, convertirse al evangelio y demostrar que hay una 
                                                identificación con el carisma hospitalario.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-postulantado"></i>
                                            <p class="title-inner">postulantado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">postulantado</p>
                                            <p class="text-p2">Esta etapa, también llamada pre-noviciado, permite continuar el discernimiento e 
                                                iniciar el proceso de formación como Hermano de San Juan de Dios. Es un período donde se favorece 
                                                el crecimiento del postulante en su dimensión humano-afectiva y vida de fe; e inicia una experiencia 
                                                en la vida consagrada en cuanto a: vida de oración, vida de comunidad, vida apostólica.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-noviciado"></i>
                                            <p class="title-inner">noviciado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">noviciado</p>
                                            <p class="text-p2">Es una etapa fundamental, los novicios viven la experiencia del encuentro personal 
                                                con Dios, disciernen, clarifican y profundizan la llamada del Señor, para poder tomar libre y 
                                                conscientemente, la decisión de seguir a Cristo Buen Samaritano, en la Orden Hospitalaria, en 
                                                condiciones suficientes de estabilidad y equilibrio espiritual. Es un tiempo donde se evidencian 
                                                las cualidades humanas y espirituales de los novicios, comprobando su intención para la profesión 
                                                de los votos religiosos.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-escolasticado"></i>
                                            <p class="title-inner">escolasticado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">escolasticado</p>
                                            <p class="text-p2">Etapa de formación teológica, profesional y consolidación vocacional, que abarca el 
                                                tiempo que va desde la primera profesión hasta la profesión solemne. En esta etapa se pretende 
                                                consolidar la opción vocacional, consiguiendo el grado de madurez que le permita comprender y vivir 
                                                su consagración en la Orden como un verdadero bien para sí mismo y para los demás: asimilando el 
                                                carisma con profundo espíritu evangélico.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>
</body>

</html>