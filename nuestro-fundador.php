<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/laClinica.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">LA CLÍNICA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <!--FUNDADOR-->
                            <div class="col-xs-12 pd-x-0 pb2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 pr3">
                                        <div class="col-xs-12 pd-x-0">
                                            <h2 class="titles-descrip">
                                                <span class="icon-san"></span>
                                                <span class="span-titlesDescrip">
                                                    san juan</span><br>de dios</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 tabs-our-founder pr-3">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <ul class="nav nav-tabs">
                                            <li class="item-tab active"><a href="#tab1" data-toggle="tab" class="link-tab">1495</a></li>
                                            <li class="item-tab"><a href="#tab2" data-toggle="tab" class="link-tab">1523</a></li>
                                            <li class="item-tab"><a href="#tab3" data-toggle="tab" class="link-tab">1538</a></li>
                                            <li class="item-tab"><a href="#tab4" data-toggle="tab" class="link-tab">1539</a></li>
                                            <li class="item-tab"><a href="#tab5" data-toggle="tab" class="link-tab">1550</a></li>
                                            <li class="item-tab"><a href="#tab6" data-toggle="tab" class="link-tab">1572</a></li>
                                            <li class="item-tab"><a href="#tab7" data-toggle="tab" class="link-tab">1586</a></li>
                                            <li class="item-tab"><a href="#tab8" data-toggle="tab" class="link-tab">1630</a></li>
                                            <li class="item-tab"><a href="#tab9" data-toggle="tab" class="link-tab">1690</a></li>
                                        </ul>
                                    </div>
                                    
                                    <div class="panel-body col-xs-12">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1495</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab2">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1523</h2>
                                                        <p class="text-internas text-border">Se une a la vida militar, iniciando un proceso de búsqueda que le lleva a regresar a Portugal 
                                                            para después trasladarse a Sevilla y de allí pasar al Norte de África. La etapa más inicial en 
                                                            su vida es confusa y se dispone de pocos datos.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab3">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1538</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab4">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1539</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab5">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1550</h2>
                                                        <p class="text-internas text-border">Se une a la vida militar, iniciando un proceso de búsqueda que le lleva a regresar a Portugal 
                                                            para después trasladarse a Sevilla y de allí pasar al Norte de África. La etapa más inicial en 
                                                            su vida es confusa y se dispone de pocos datos.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab6">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1572</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab7">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1586</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab8">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1630</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab9">
                                                <div class="info-tab">
                                                    <div class="content-img-tab col-xs-12 col-md-11">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/nuest-fund-1515.jpg" alt="">
                                                    </div>
                                                    <div class="description-flotant">
                                                        <h2 class="year-tab color-primary">1690</h2>
                                                        <p class="text-internas text-border">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente eos soluta, labore quaerat officiis ullam mollitia, voluptas laboriosam recusandae nisi facilis, dolor eius iure nihil autem reiciendis suscipit voluptate rerum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--SÍNTESIS DE SU OBRA-->
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 pr3">
                                        <div class="col-xs-12 pd-x-0">
                                            <h2 class="titles-descrip">
                                                <span class="icon-san"></span>
                                                <span class="span-titlesDescrip">
                                                    síntesis</span><br>de su obra</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 work-synthesis">
                                <div class="row">
                                    <div class="content-synthesis">
                                        <div class="img-synthesis hidden-xs hidden-sm col-xs-8">
                                            <img src="assets/images/internas/la-clinica/sintesis.jpg" alt="" class="img-cover">
                                        </div>
                                        <ul class="list-synthesis col-xs-12 col-md-4">
                                            <li class="item-synthesis col-xs-6 col-md-12">
                                                <i class="icons-synthesis icon-solidario"></i>
                                                <h2 class="ttl-synthesis">solidario</h2>
                                                <p class="text-internas">Desde una especial sensibilidad social, sale al encuentro de las personas necesitadas.</p>
                                            </li>
                                            <li class="item-synthesis col-xs-6 col-md-12">
                                                <i class="icons-synthesis icon-equitativo"></i>
                                                <h2 class="ttl-synthesis">equitativo</h2>
                                                <p class="text-internas">Actua con absoluta universalidad, todo necesitado tiene derecho a ser atendido.</p>
                                            </li>
                                            <li class="item-synthesis col-xs-6 col-md-12">
                                                <i class="icons-synthesis icon-proactivo"></i>
                                                <h2 class="ttl-synthesis">proactivo</h2>
                                                <p class="text-internas">Solicitud de recursos a toda la sociedad, sin distinción. Llamada a la solidaridad sin fronteras.</p>
                                            </li>
                                            <li class="item-synthesis col-xs-6 col-md-12">
                                                <i class="icons-synthesis icon-lider"></i>
                                                <h2 class="ttl-synthesis">líder</h2>
                                                <p class="text-internas">Aglutina a un grupo de personas que le ayudan, suplen y dan continuidad a su obra.</p>
                                            </li>
                                            <li class="item-synthesis col-xs-6 col-md-12">
                                                <i class="icons-synthesis icon-escolasticado"></i>
                                                <h2 class="ttl-synthesis">íntegro</h2>
                                                <p class="text-internas">Atención integral a las personas enfermas y necesitadas, respetando su dignidad y derechos.</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>
<script>

console.log(window.scrollY);
</script>
</body>

</html>