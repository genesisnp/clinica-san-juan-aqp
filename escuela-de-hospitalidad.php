<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner">
        <img class="img-banner" src="/assets/images/banner/red.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">FORMACIÓN</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic formation">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <!--  PARTICIPACIÓN -->
                            <div class="col-xs-12 pd-x-0">
                                <h2 class="titles-descrip mb-0"><span class="icon-san"></span>
                                    <span class="span-titlesDescrip">
                                    escuela</span><br>de hospitalidad</h2>
                            </div>
                            <div class="col-xs-12 pd-x-0 q-es pr-3">
                                <div class="row">
                                    <div class="col-xs-12 col-md-5 descrip-formation pr-4">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">Participación</h2>
                                            </div>
                                            <p class="text-p2 text-justify">La Escuela de Hospitalidad tiene dos módulos 
                                                de 12 meses cada uno. El contenido desarrollado son temas y actividades 
                                                que permiten la comprensión e incorporación del Carisma Juandediano.</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <div class="content-img-esc-hosp">
                                            <img class="img-cover" src="assets/images/internas/la-clinica/escuela-de-hospitalidad.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- OBJETIVOS -->
                            <div class="col-xs-12 obj-esc-hosp">
                                <h2 class="sub-ttl-flotant ttl-presence color-primary">Nuestros objetivos</h2>
                                <div class="wrapper-card-repeat">
                                    <div class="cols-value">
                                        <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-conocimiento.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-conocimiento"></i>
                                                        <p class="title-inner">conocimiento</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-conocimiento.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-conocimiento"></i>
                                                        <p class="title-inner">conocimiento</p>
                                                        <p class="text-p2">Que la persona logre los conocimientos suficientes relacionados con: Vida del 
                                                            Fundador, Carisma Hospitalario y Misión de la Orden.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-identificacion"></i>
                                                        <p class="title-inner">identificación</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-identificacion"></i>
                                                        <p class="title-inner">identificación</p>
                                                        <p class="text-p2">Que a través de estos conocimientos vivencie una identificación con los valores del Carisma Hospitalario. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-hospitalidad"></i>
                                                        <p class="title-inner">hospitalidad</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-hospitalidad"></i>
                                                        <p class="title-inner">hospitalidad</p>
                                                        <p class="text-p2">Que la identificación con el Carisma de la Hospitalidad genere responsabilidad y 
                                                            compromiso en la transmisión de los valores de la Orden y el sostenimiento de la Misión. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-compromiso"></i>
                                                        <p class="title-inner">compromiso</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                                    <div class="inner">
                                                        <i class="icon-card icon-compromiso"></i>
                                                        <p class="title-inner">compromiso</p>
                                                        <p class="text-p2">Que este compromiso y responsabilidad garantice una adecuada proyección social del Carisma y Misión de la Orden. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-formation.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>
</body>

</html>