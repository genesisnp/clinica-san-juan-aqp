<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/laClinica.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">LA CLÍNICA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <!--SECCION MENSAJEROS DE LA SALUD-->
                            <section class="sct-hospital-vocation">
                                <div class="container-fluid pd-x-0">
                                    <div class="row">
                                        <div class="col-xs-12 pd-x-0">
                                            <h2 class="titles-descrip"><span class="icon-san"></span>
                                                <span class="span-titlesDescrip">
                                                ser religioso</span><br>de san juan dios</h2>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="us-oh row religioso-sjd">
                                                <div class="img-us-oh float-right col-xs-12 col-md-10 pd-x-0">
                                                    <img class="img-cover" src="assets/images/internas/la-clinica/vocacion-hospitalaria1.jpg" alt="">
                                                </div>
                                                <div class="description-flotant descp-flot-l dscp-2">
                                                    <p class="text-border text-p2">La Orden Hospitalaria la conformamos Hermanos consagrados a Dios, que seguimos a Jesús, al estilo de nuestro fundador San Juan de Dios. Por ello, vivimos en comunidad y nos dedicamos a Evangelizar desde el carisma de la Hospitalidad, en el ámbito de la salud y poniéndonos al servicio de aquellos que experimentan pobreza, sufrimiento, marginación y otras carencias. Prolongando de esta manera el amor compasivo y misericordioso de Dios, entre los hombres.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- VOCACIÓN -->
                                        <div class="col-xs-12 pd-x-0">
                                            <div class="row content-mv flex-reverseMovil">
                                                <div class="col-xs-12 col-sm-6 col-md-8">
                                                    <div class="img-mv-oh">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/vocacion-hospitalaria2.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 wrapper-mv pl-6">
                                                    <div class="row">
                                                        <div class="vocacion">
                                                            <h2 class="sub-ttl-flotant color-primary">Vocación</h2>
                                                            <p class="text-internas text-justify">La vocación es iniciativa y un don recibido de Dios, pero es también una tarea que nos compromete. El Señor nos amó y llamó primero, y nosotros le hemos respondido siguiéndolo. Es por habernos sentido amados por Él, que hemos acogido su invitación anteponiendo a otros intereses que el mundo nos ofrece. Orientamos nuestra vida para desarrollar un proyecto de vida personal y comunitario de seguimiento a Jesús, desde el carisma de la Hospitalidad.</p>
                                                            <p class="text-internas text-justify">Para nosotros los Religiosos de San Juan de Dios, la Hospitalidad se convierte en una manera concreta de vivir y seguir a Jesús. Ser hospitalarios es todo un estilo de vida que nos impulsa a acoger sin límites a todo ser humano de este mundo marcado por la hostilidad.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- SAN JUAN DE DIOS -->
                                        <div class="col-xs-12 pd-x-0">
                                            <div class="row content-mv obrs-apos">
                                                <div class="col-xs-12 col-md-4 wrapper-mv">
                                                    <div class="row">
                                                        <div class="san-juan-de-dios">
                                                            <h2 class="sub-ttl-flotant color-primary">San Juan de Dios</h2>
                                                            <p class="text-internas text-justify">De Juan de Dios conservamos pocos escritos, pero en ellos él nos expresa la profundidad y riqueza de su experiencia de Dios. Y nos invita a vivir esta misma experiencia y por ello nos dice: “Si conocieras lo grande que es la misericordia de Dios, nunca dejarías de hacer el bien mientras pudieses”. La grandeza del amor de Dios que lo impregna todo, nos conduce al amor mismo en nuestros hermanos necesitados. Por eso Juan nos llama a “Tener siempre caridad, porque donde hay caridad, hay Dios, aunque Dios en todo lugar está”.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="img-obras-apost">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/vocacion-hospitalaria3.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- PROGRAMA DE ACOMPAÑAMIENTO VOCACIONAL -->
                                        <div class="col-xs-12">
                                            <div class="programa-av flex-reverseMovil">
                                                <div class="img-programa-av">
                                                    <img class="img-cover" src="assets/images/internas/la-clinica/vocacion-hospitalaria4.jpg" alt="">
                                                </div>
                                                <div class="description-flotant descp-flot-r">
                                                    <h2 class="sub-ttl-flotant color-primary">Programa de<br>acompañamiento vocacional</h2>
                                                    <ul>
                                                        <li class="text-internas fleli text-justify">Encuentros y entrevistas personales con el Hermano Acompañante.</li>
                                                        <li class="text-internas fleli text-justify">Inserción periódica o permanente en la Comunidad asignada para esta experiencia.</li>
                                                        <li class="text-internas fleli text-justify">Participación en convivencias vocacionales con otros aspirantes.</li>
                                                        <li class="text-internas fleli text-justify">Profundización de la vida cristiana y lectura de los documentos de la Iglesia y de la Orden.</li>
                                                        <li class="text-internas fleli text-justify">Compromiso con realidades de pobreza, enfermedad, sufrimiento y exclusión.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- ACOMPAÑAMIENTO VOCACIONAL INTERNO -->
                                        <div class="col-xs-12 pd-x-0">
                                            <div class="row content-mv">
                                                <div class="col-xs-12 col-md-4 wrapper-mv">
                                                    <div class="row">
                                                        <div class="a-vocacionalInterno">
                                                            <h2 class="sub-ttl-flotant color-primary">Acompañamiento vocacional interno</h2>
                                                            <p class="text-internas text-justify">Esta etapa inicial es un tiempo caracterizado por el mutuo conocimiento entre el candidato y la Orden. Es una experiencia en el que el joven discierne si tiene cualidades para ser religioso. Para ello el candidato se inserta de manera periódica o permanente en una Comunidad nuestra. Es un tiempo para revisar y profundizar en la propia experiencia humana y cristiana, a través del compartir de la historia la historia personal e historia de fe.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="img-obras-apost">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/vocacion-hospitalaria5.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- PROCESO DE FORMACIÓN -->
                                        <div class="col-xs-12">
                                            <h2 class="titles-descrip"><span class="icon-san"></span>
                                                <span class="span-titlesDescrip">
                                                Proceso de Formación del Hermano</span><br>de San Juan de Dios
                                            </h2>
                                            <h3 class="sub-ttl-flotant color-primary">Formación inicial</h3>
                                        </div>
                                        <div class="col-xs-12 card-process-formation">
                                            <div class="wrapper-card-repeat">
                                                <div class="cols-value">
                                                    <div class="col-xs-12 col-sm-6 col-md-4 cols" ontouchstart="this.classList.toggle('hover');">
                                                        <div class="containers">
                                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                                                <div class="inner">
                                                                    <i class="icon-card icon-postulantado"></i>
                                                                    <p class="title-inner">Postulantado</p>
                                                                </div>
                                                            </div>
                                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                                                <div class="inner">
                                                                    <p class="title-inner">Postulantado</p>
                                                                    <p class="text-p2">Es un período de dos años, en donde se favorece el crecimiento del postulante en su madurez humana y afectiva, en su vida de fe y oración, en el servicio a los demás, a través de la cercanía a realidades sanitarias y sociales. Esta experiencia la vivirá en comunidad para que pueda descubrir también la riqueza de la vida fraterna. Esta propuesta facilitará que el candidato acompañado por un Hermano descubra su idoneidad a nuestro carisma particular y a la vida consagrada.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-6 col-md-4 cols" ontouchstart="this.classList.toggle('hover');">
                                                        <div class="containers">
                                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                                                <div class="inner">
                                                                    <i class="icon-card icon-noviciado"></i>
                                                                    <p class="title-inner">Noviciado</p>
                                                                </div>
                                                            </div>
                                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                                                <div class="inner">
                                                                    <p class="title-inner">Noviciado</p>
                                                                    <p class="text-p2">Es una etapa fundamental, los novicios viven la experiencia del encuentro personal 
                                                                        con Dios, disciernen, clarifican y profundizan la llamada del Señor, para poder tomar libre y 
                                                                        conscientemente, la decisión de seguir a Cristo Buen Samaritano, en la Orden Hospitalaria, en 
                                                                        condiciones suficientes de estabilidad y equilibrio espiritual. Es un tiempo donde se evidencian 
                                                                        las cualidades humanas y espirituales de los novicios, comprobando su intención para la profesión 
                                                                        de los votos religiosos.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-6 col-md-4 cols" ontouchstart="this.classList.toggle('hover');">
                                                        <div class="containers">
                                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                                                <div class="inner">
                                                                    <i class="icon-card icon-escolasticado"></i>
                                                                    <p class="title-inner">Escolasticado</p>
                                                                </div>
                                                            </div>
                                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                                                <div class="inner">
                                                                    <p class="title-inner">Escolasticado</p>
                                                                    <p class="text-p2">Etapa de formación teológica, profesional y consolidación vocacional, que abarca el 
                                                                        tiempo que va desde la primera profesión hasta la profesión solemne. En esta etapa se pretende 
                                                                        consolidar la opción vocacional, consiguiendo el grado de madurez que le permita comprender y vivir 
                                                                        su consagración en la Orden como un verdadero bien para sí mismo y para los demás: asimilando el 
                                                                        carisma con profundo espíritu evangélico.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- FORMACIÓN PERMANENTE -->
                                        <div class="col-xs-12 pd-x-0 formacion-permanente">
                                            <div class="row content-mv">
                                                <div class="col-xs-12 col-md-4 wrapper-mv">
                                                    <div class="row">
                                                        <div class="formacion-permanente">
                                                            <h2 class="sub-ttl-flotant color-primary">Formación permamente</h2>
                                                            <p class="text-internas text-justify">Todo religioso hospitalario de Votos Solemnes debe mantenerse actualizado y capacitado para responder a las exigencias de toda Vida y Misión Hospitalaria. Por eso, aunque llegue a su término la formación inicial el Hermano de San Juan de Dios ha de mantenerse siempre disponible a aprender y adquirir nuevos conocimientos.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="img-obras-apost">
                                                        <img class="img-cover" src="assets/images/internas/la-clinica/vocacion-hospitalaria9.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>