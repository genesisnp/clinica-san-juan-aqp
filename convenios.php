<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/informacionAlUsuario.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">INFORMACIÓN AL USUARIO</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                nuestros</span><br>convenios</h2>
                                    </div>

                                    <!-- ASEGURADORAS -->
                                    <div class="row">
                                        <h3 class="p-internas tabfun4">Aseguradoras:</h3>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/1.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/1.png" alt="logo">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/2.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/2.png" alt="logo">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/3.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/3.png" alt="logo">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/4.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/4.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/5.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/5.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/6.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/6.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/7.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/7.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/aseguradoras/black-white/8.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/aseguradoras/color/8.png" alt="logo">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- EMPRESAS -->
                                    <div class="row">
                                        <h3 class="p-internas tabfun4">Empresas:</h3>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/1.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/1.png" alt="logo">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/2.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/2.png" alt="logo">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/3.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/3.png" alt="logo">
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/4.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/4.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/5.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/5.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/6.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/6.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/7.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/7.png" alt="logo">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInRight" data-wow-delay="0s">
                                            <div class="logo-convenio">
                                                <img class="logo-convenio_bw" src="/assets/images/convenios/empresas/black-white/8.png" alt="logo">
                                                <img class="logo-convenio_color" src="/assets/images/convenios/empresas/color/8.png" alt="logo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-inf-usuario.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>