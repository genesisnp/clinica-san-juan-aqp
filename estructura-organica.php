<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/laClinica.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">LA CLÍNICA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <!-- ESTRUCTURA DE LA ORDEN HOSPITALARIA -->
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                estructura</span><br>de la orden hospitalaria</h2>
                                    </div>
                                    <div class="col-xs-12 padd-0 wow fadeInUp" data-wow-delay="0.25s">
                                        <div class="us-oh pr-3">
                                            <div class="col-xs-11 img-us-oh float-right pr-1">
                                                <img class="img-cover" src="assets/images/internas/la-clinica/estruct-organica1.jpg" alt="">
                                            </div>
                                            <div class="description-flotant dscp-2">
                                                <p class="text-border text-p2">La Orden Hospitalaria se divide, a nivel mundial, en 21 Provincias, 1 Vice-Provincia, 1 Delegación General y 7 Delegaciones Provinciales.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PROVINCIAS COMUNIDADES -->
                                    <div class="col-xs-12 pd-x-0">
                                        <div class="row content-mv">
                                            <div class="col-xs-12 col-sm-6 col-md-8 wow fadeInUp" data-wow-delay="0.25s">
                                                <div class="img-mv-oh">
                                                    <img class="img-cover" src="assets/images/internas/la-clinica/contribucion-oh.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 wrapper-mv pl-6 wow fadeInRight" data-wow-delay="0.75s">
                                                <div class="row">
                                                    <div class="mission">
                                                        <h2 class="sub-ttl-flotant color-primary">Provincia</h2>
                                                        <p class="text-internas text-justify">Territorio constituido por un cierto número de Obras Apostólicas 
                                                            y Comunidades de Hermanos, entre las cuales existe una especial relación de hermandad y servicio 
                                                            apostólico, bajo la guía de un Superior Mayor llamado Provincial. </p>
                                                    </div>
                                                    <div class="vission">
                                                        <h2 class="sub-ttl-flotant color-primary">Comunidades</h2>
                                                        <p class="text-internas text-justify">Constituidas por un número de Hermanos para el ejercicio de una 
                                                            misión concreta y la participación en la vida fraterna, bajo la guía de un Superior Local.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- OBRAS APOSTÓLICAS -->
                                    <div class="col-xs-12 pd-x-0">
                                        <div class="row content-mv obrs-apos">
                                            <div class="col-xs-12 col-md-4 wrapper-mv wow fadeInRight" data-wow-delay="0.25s">
                                                <div class="row">
                                                    <div class="mission">
                                                        <h2 class="sub-ttl-flotant color-primary">Obras<br>apostólicas</h2>
                                                        <p class="text-internas text-justify">Son Estructuras en función de una determinada misión asistencial, 
                                                            bajo la dirección de un Gerente, con autoridad delegada del Hermano Provincial. </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-8 wow fadeInUp" data-wow-delay="0.75s">
                                                <div class="img-obras-apost">
                                                    <img class="img-cover" src="assets/images/internas/la-clinica/estruct-organica3.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <!-- ESTRUCTURA ORGANICA MAPA -->
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                estructura</span><br>orgánica de la provincia</h2>
                                        <div class="estructura-organica_svg wow zoomIn" data-wow-delay="0.25s">
                                            <img src="assets/images/internas/la-clinica/estructura-organica.svg" alt="Estructura Orgánica">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>