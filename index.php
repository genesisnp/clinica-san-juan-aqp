<?php
    include 'src/includes/header.php'
?>
<main id="fullpage" class="main-home">
    <!--CARROUSEL HOME-->
    <section class="section sct-carousel-home pos-rel container-fluid">
        <div class="slider-home hg-100 owl-carousel">
            <div class="item hg-100">
                <video autoplay="" loop="" muted="" class="video">
                    <source type="video/mp4" src="assets/images/video/csjd_arequipa.mp4">
                </video>
            </div>
            <div class="item hg-100">
                <img src="assets/images/noticia1.jpg" alt="">
            </div>
            <div class="item hg-100">
                <img src="assets/images/noticia1.jpg" alt="">
            </div> 
        </div>
        <a href="#section1"><img class="arrow-ancla" src="assets/images/icons/arrowAncla.svg" alt=""></a>
    </section>

    <!-- SECCION EN QUE TE PODEMOS AYUDAR -->
    <section class="sct-filter-quotes section" id="section1">
        <div class="sec2">
            <div class="container">
                <h1 class="text-center colorPrimary titlesBig mayus">¿En qué te podemos ayudar?</h1>
                <?php
                    include 'src/includes/filtro.php'
                ?>
                <div class="col-xs-12 wrapper-download-quotes pd-x-0">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 wrapper-quotes">
                            <div class="content-quotes">
                                <i class="icon-citas"></i>
                                <h3 class="titlesBig">Citas en Línea</h3>
                                <p class="p-internas small">Ahora ya puede sacar tus citas y pagarlo en<br>línea sin necesidad
                                    de ir a la
                                    clínica.</p>
                                <form action="#" class="form flex jusContentCenter flex-column" method="post"
                                    id="form-quotes">
                                    <div class="form__wrapper mb0-5">
                                        <input type="text" class="form__input" id="document" name="document">
                                        <label class="form__label text-center">
                                            <span class="form__label-content">Nro.de Documento</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper mb0-5">
                                        <input type="text" class="form__input" id="lastname" name="lastname">
                                        <label class="form__label text-center">
                                            <span class="form__label-content">Contraseña</span>
                                        </label>
                                    </div>
                                </form>
                                <button class="btn-primary btn btn-inSesion">Iniciar Sesión</button>
                                <a href="#" class="internasBold rg-Cntr reg">Regístrate</a>
                                <a href="#" class="p-internas rg-Cntr cambCont sinb">¿Olvidaste tu contraseña?</a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 wrapper-app d-flex">
                            <div class="content-app">
                                <i class="icon-app"></i>
                                <h3 class="titlesBig colorPrimary text-center">Descarga<br>nuestra App</h3>
                                <ul class="list-stepsApp text-center small">
                                    <li class="item-stepsApp"><span class="span-stepsApp p-internas">Realiza el pago de
                                            tus citas de forma segura.</span></li>
                                    <li class="item-stepsApp"><span class="span-stepsApp p-internas">Ahorra tu
                                            tiempo.</span></li>
                                    <li class="item-stepsApp"><span class="span-stepsApp p-internas">Elige al médico,
                                            especialista y hora de tu preferencia.</span></li>
                                </ul>
                                <div class="row">
                                    <a href="#" class="col-xs-6"><img class="img-downloadApp w-100" src="assets/images/googleplay.png" alt=""></a>
                                    <a href="#" class="col-xs-6"><img class="img-downloadApp w-100" src="assets/images/appStore.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--SECCION ESPECIALIDADES-->
    <section class="sct-specialties section" id="section2">
        <div class="container posrel">
        <h2 class="passer colorPrimary titlesBig mayus">SERVICIOS Y ESPECIALIDADES</h2>
        <?php
            include 'src/includes/servicios-y-especialidades.php'
        ?>
        </div>
    </section>
    
    <!--SECCION NOVEDADES-->
    <section class="section sct-news bgInternas">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ttl-news color-primary text-center titles-big"><h2>NOVEDADES</h2></div>
                <div class="col-xs-6 card-big">
                    <div class="carrousel-news owl-carousel">
                        <div class="item-carrousel-news">
                            <div class="grid-news hg-100">
                                <div class="sombra"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia General
                                                - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="item-carrousel-news">
                            <div class="grid-news hg-100">
                                <div class="sombra"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/internas/mensajeros1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia General
                                                - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="item-carrousel-news">
                            <div class="grid-news hg-100">
                                <div class="sombra"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/internas/mensajeros2.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia General
                                                - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 card-peqs">
                    <div class="row">
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/ecuador.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/venezuela.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/peru.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--SECCION FORMULARIO-->
    <section class="section form-and-part">
        <?php
            include 'src/includes/forms.php'
        ?>
        <!-- MODAL POLITICAS DE PRIVACIDAD -->
        <?php
            include 'src/includes/modal-pol-priv.php'
        ?>
        <!--SECCION PARTICIPA-->
        <?php
            include 'src/includes/sct-hazteHome.php'
        ?>
    </section>
    
    <?php
        include 'src/includes/footer.php'
    ?>
</main>

<script src="assets/js/libraries/fullpage.js"></script>
<script src="assets/js/modal-form.js"></script>
<script src="assets/js/libraries/slick.js"></script>
<script>
    if (screen && screen.width > 1300) {
        let fullpageDiv = $('#fullpage');
        if (fullpageDiv.length) {
            fullpageDiv.fullpage({
                scrollBar: true,
                scrollOverflow: true,
                verticalCentered: true,
                navigation: true,
                navigationPosition: 'right',
                navigationTooltips: ['Inicio', '¿En qué te podemos ayudar?', 'Servicios y Especialidades', 'Novedades', 'Contacto'],
                afterRender: function () {

                }
            });
        }
    }
</script>

<script>
    /* Scroll */
    jQuery('a').click(function(){
        jQuery('html, body').animate({
            scrollTop: jQuery( jQuery(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
</script>

<script>
    $(document).ready(function () {
        $('.tabs span').click(function () {
            var tab_id = $(this).attr('data-tab');

            $('.tabs span').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        })
    })
</script>

<script>
    $('.row.borbg .op5s').mouseover(function (e) {     
        //get selected href
        var data2 = $(this).attr('data-target');    
        
        //set all nav tabs to inactive
        $('.row.borbg ').removeClass('active');
        
        //get all nav tabs matching the href and set to active
        $('.row.borbg  .op5s[data-target="'+data2+'"]').addClass('active');

        //active tab
        $('.tab-img').removeClass('active');
        $('.tab-img'+data2).addClass('active');
    });
    $('.row.borbg .op5s').mouseout(function (e) {     
        //get selected href
        var data2 = $(this).attr('data-target');    
        
        //set all nav tabs to inactive
        $('.row.borbg ').removeClass('active');
        
        //get all nav tabs matching the href and set to active
        $('.row.borbg  .op5s[data-target="'+data2+'"]').removeClass('active');

        //active tab
        $('.tab-img').removeClass('active');
        $('.tab-img'+data2).removeClass('active');
    })
</script>

<script>
    $(document).ready(function () {
        $('.slider-servicios').slick({
            dots: false,
            slidesPerRow: 1,
            rows: 6,
            arrows: true,
            responsive: [{
                breakpoint: 1400,
                settings: {
                slidesPerRow: 4,
                rows: 2,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                slidesPerRow: 3,
                rows: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                slidesPerRow: 2,
                rows: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                slidesPerRow: 1,
                rows: 3,
                }
            }
            ]
        });

        var $slider = $('.slider-servicios');

        if ($slider.length) {
            var currentSlide;
            var slidesCount;
            var sliderCounter = document.createElement('div');
            sliderCounter.classList.add('slider__counter');
            
            var updateSliderCounter = function(slick, currentIndex) {
                currentSlide = slick.slickCurrentSlide() + 1;
                slidesCount = slick.slideCount;
                $(sliderCounter).text(currentSlide + '/' +slidesCount)
            };

            $slider.on('init', function(event, slick) {
                $slider.append(sliderCounter);
                updateSliderCounter(slick);
            });

            $slider.on('afterChange', function(event, slick, currentSlide) {
                updateSliderCounter(slick, currentSlide);
            });

            $slider.slick();
        }

    });
</script>

</body>

</html>