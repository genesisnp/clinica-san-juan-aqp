<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/informacionAlUsuario.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">INFORMACIÓN AL USUARIO</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0 paddCustomMovil0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                tips</span><br>de salud</h2>
                                    </div>
                                </div>

                                <div class="row pb7 tip">
                                    <div class=" pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip1.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Diabetes</h4>
                                                        <p class="regul">La diabetes es una enfermedad crónica que se origina porque el ...</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip2.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Lavado de manos</h4>
                                                        <p class="regul">Son muchas las formas de prevenir enfermedades, pero podemos asegurarte ....</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip3.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Primeros auxilios</h4>
                                                        <p class="regul">Si te encuentras con una persona inconsciente, con una hemorragia, con un atragantamiento… </p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip4.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Salud bucal</h4>
                                                        <p class="regul">La buena higiene bucal proporciona una boca que luce y huele saludablemente ....</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip5.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Neumonía</h4>
                                                        <p class="regul">La neumonía es una infección que inflama los sacos aéreos de uno ....</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class=" pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip6.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Lunares</h4>
                                                        <p class="regul">Los lunares suelen ser rosados, morenos o marrones. Pueden ser planos o elevados ....</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class=" pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip1.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Diabetes</h4>
                                                        <p class="regul">La diabetes es una enfermedad crónica que se origina porque el ...</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip2.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Lavado de manos</h4>
                                                        <p class="regul">Son muchas las formas de prevenir enfermedades, pero podemos asegurarte ....</p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="pb3 col-xs-12 col-sm-4 col-lg-4">
                                        <div class="grid-news grid-staff">
                                            <figure class="wrapper-news">
                                                <div class="content-img-staff">
                                                    <img class="fondo-image" src="assets/images/internas/inf-usuario//tip3.jpg" alt="img18" />
                                                </div>
                                                <figcaption>
                                                    <div class="title-news ttn">
                                                        <h4 class="regul">Primeros auxilios</h4>
                                                        <p class="regul">Si te encuentras con una persona inconsciente, con una hemorragia, con un atragantamiento… </p>
                                                    </div>
                                                </figcaption>
                                                
                                                <a class="link-cardGrid lkcg" href="medic-post.php"><div class="vm"></div></a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-inf-usuario.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>