<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/laClinica.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">LA CLÍNICA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                principios</span><br>y valores</h2>
                                    </div>
                                    <div class="col-xs-12 pd-x-0">
                                        <div class="row content-mv pr-3">
                                            <div class="col-xs-12 col-sm-5 col-md-4 wrapper-mv">
                                                <div class="row">
                                                    <div class="mission">
                                                        <h2 class="sub-ttl-flotant color-primary">Valores</h2>
                                                        <p class="text-p2 text-justify">Participando en la formación de los profesionales en la región sur del país, autogenerando recursos económicos con la atención de pacientes privados en compañías de seguros con calidad, calidez y excelencia.</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-8">
                                                <div class="img-mv-oh">
                                                    <img class="img-cover" src="assets/images/internas/la-clinica/principios.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- NUESTROS VALORES -->
                            <div class="col-xs-12">
                                <h2 class="sub-ttl-flotant ttl-presence color-primary">Nuestros<br>valores como institución</h2>
                                <div class="wrapper-card-repeat">
                                    <div class="col-5flex" ontouchstart="this.classList.toggle('hover');">
                                        <div class="containers">
                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-hospitalidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Hospitalidad</p>
                                                </div>
                                            </div>

                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-hospitalidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Hospitalidad</p>
                                                    <p class="text-p2">Es nuestro valor central, que se expresa y se concreta en los cuatro valores guía, es decir calidad, respeto, responsabilidad y espiritualidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-5flex" ontouchstart="this.classList.toggle('hover');">
                                        <div class="containers">
                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-calidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Calidad</p>
                                                </div>
                                            </div>

                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-calidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Calidad</p>
                                                    <p class="text-p2">La excelencia profesional en la atención integral al paciente, poniendo a su disposición los medios técnicos, humanos y espirituales que precise en cada momento.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-5flex" ontouchstart="this.classList.toggle('hover');">
                                        <div class="containers">
                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-respeto.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Respeto</p>
                                                </div>
                                            </div>

                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-respeto.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Respeto</p>
                                                    <p class="text-p2">Consideramos al usuario de nuestros Centros en su dimensión humana como el centro de nuestra atención, teniendo en cuenta sus derechos y decisiones e implicando en el proceso a los familiares. Promovemos la justicia social y los derechos civiles y humanos.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-5flex" ontouchstart="this.classList.toggle('hover');">
                                        <div class="containers">
                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-espiritualidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Espiritualidad</p>
                                                </div>
                                            </div>

                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-espiritualidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Espiritualidad</p>
                                                    <p class="text-p2">Acoge como valor el ofrecimiento de atención espiritual hacia todos los usuarios, pacientes, familiares y profesionales -sea cual sea su confesionalidad- considerando sus necesidades religiosas y contribuyendo, de esta manera, a la Evangelización.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-5flex" ontouchstart="this.classList.toggle('hover');">
                                        <div class="containers">
                                            <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-responsabilidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Responsabilidad</p>
                                                </div>
                                            </div>

                                            <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-responsabilidad.jpg)">
                                                <div class="inner">
                                                    <p class="title-inner">Responsabilidad</p>
                                                    <p class="text-p2">Nuestra obligación con los usuarios el medio ambiente y hacia los ideales de San Juan de Dios y de la Orden. Siendo capaces, a su vez, de aplicar la ética y una justa distribución de los recursos de los que disponemos en todas las actividades que llevamos a cabo para la adecuada sostenibilidad de los Centros.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--NUESTROS PRINCIPIOS-->
                            <div class="col-xs-12">
                                <h2 class="sub-ttl-flotant ttl-presence color-primary">Nuestros principios</h2>
                            </div>
                            <div class="col-xs-10 float-right pd-x-0">
                                <div class="wrapper-slide-beginning ">
                                    <div class="slide-beginning container-fluid pd-x-0 owl-carousel" id="slide-beginning">
                                        <!-- Item 1 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios1.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Afirmamos que el centro de interés es la persona asistida.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 2 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios2.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Promovemos y defendemos los derechos del enfermo y necesitado, teniendo en cuenta su dignidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 3 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios3.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Nos comprometemos en la defensa y promoción de la vida humana; Desde su concepción hasta la muerte.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 4 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios1.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Afirmamos que el centro de interés es la persona asistida.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 5 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios2.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Promovemos y defendemos los derechos del enfermo y necesitado, teniendo en cuenta su dignidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 6 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios3.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Nos comprometemos en la defensa y promoción de la vida humana; Desde su concepción hasta la muerte.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 7 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios1.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Afirmamos que el centro de interés es la persona asistida.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 8 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios2.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Promovemos y defendemos los derechos del enfermo y necesitado, teniendo en cuenta su dignidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Item 9 -->
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-principios3.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Nos comprometemos en la defensa y promoción de la vida humana; Desde su concepción hasta la muerte.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="indicator"></div>
                                </div>
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>
<script>
   
</script>
</body>

</html>