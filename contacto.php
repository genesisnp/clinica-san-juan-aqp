<?php
    include 'src/includes/header.php'
?>
    <main id="my-scrollbar" data-scrollbar>
        <section id="parallax" class="sct-banner scroll">
            <div class="degrade-int"></div>
            <img class="img-banner" src="/assets/images/banner/contacto-final.jpg" alt="Contacto">
            <div class="content-title-banner container">
                <h1 class="titleBanner text-uppercase">CONTACTO</h1>
            </div>
        </section>

        <section class="wrapper-info-theClinic pt3 pb7 contacto-into bg-white">
            <div class="container">
                <!-- Title -->
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="title-contacto">CONTÁCTANOS</h2>
                    </div>
                </div>

                <!-- Info Contact -->
                <div class="row">
                    <div class="col-xs-12 col-md-6 wow fadeInLeft" data-wow-delay="0s">
                        <!-- Texto -->
                        <div class="row pb2">
                            <div class="col-xs-12">
                                <p class="text-internas text-justify">Ante cualquier duda que usted presente, estaremos siempre para atenderlo y guiarlo. Envíenos sus dudas mediante el formulario completando los campos solicitados.</p>
                            </div>
                        </div>

                        <!-- Formulario -->
                        <form action="#" class="form row pb3" method="post" id="form-contact">
                            <div class="form__wrapper col-xs-12 col-lg-12">
                                <input type="text" class="form__input" id="name" name="name">
                                <label class="form__label">
                                    <span class="form__label-content">Nombres:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-xs-12 col-lg-12">
                                <input type="text" class="form__input" id="lastname" name="lastname">
                                <label class="form__label">
                                    <span class="form__label-content">Apellidos:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-xs-12 col-lg-6">
                                <input type="text" class="form__input" id="phone" name="phone">
                                <label class="form__label">
                                    <span class="form__label-content">Teléfono:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-xs-12 col-lg-6">
                                <input type="email" class="form__input" id="email" name="email">
                                <label class="form__label">
                                    <span class="form__label-content">Email:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-xs-12 col-lg-12">
                                <textarea class="form__input form_textarea" id="textarea"
                                    name="textarea"></textarea>
                                <label class="form__label">
                                    <span class="form__label-content">Mensaje:</span>
                                </label>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <div class="checkbox">
                                    <label class="font-titles-md label-pol">
                                        <input type="checkbox" /><i class="helper"></i><span>He leído y acepto <span class="span-pol internasBold colorPrimary btn-modals">la
                                                Política de Privacidad.</span></span>
                                    </label>
                                </div>
                                <div class="btn-container">
                                    <button type="submit" name="submit" class="btn internasBold btn-send" id="btn-send-form">ENVIAR</button>
                                </div>
                            </div>
                        </form>

                        <!-- Datos -->
                        <div class="datos-contacto row pt3">
                            <div class="col-xs-12 col-sm-3 info-form">
                                <h4 class="subtitle-contacto">Teléfono:</h4>
                                <div class="flex lineH2">
                                    <div class="contentIconForm float-left"><i class="icon-form icon-phone-bg"></i></div>
                                    <p class="p-internas sinb textBold-contacto">(+054) 382400</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 info-form">
                                <h4 class="subtitle-contacto">Correo:</h4>
                                <div class="flex lineH2">
                                    <div class="contentIconForm float-left"><i class="icon-form icon-sobre"></i></div>
                                    <p class="p-internas sinb">info@sanjuandediosaqp.com</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 info-form">
                                <h4 class="subtitle-contacto">Dirección:</h4>
                                <div class="flex lineH2">
                                    <div class="contentIconForm float-left"><i class="icon-form icon-ubication"></i></div>
                                    <p class="p-internas sinb">Av. Ejército 1020 - Cayma</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                        <img src="assets/images/internas/contacto-edificio.jpg" alt="Contacto Edificio" class="w-100 contacto_imgEdificio">
                    </div>
                </div>

                <!-- Map -->
                <div class="row pt5 contacto_mapa wow zoomIn">
                    <div class="col-xs-12">
                        <div class="mapa">
                            <div class="wrap-iframe">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3827.712757062274!2d-71.55227198452101!3d-16.38858844221698!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a69f559a8b1%3A0xfb7db7d140da69dd!2sAv.%20Ejercito%201020%2C%20Cayma%2004017!5e0!3m2!1ses-419!2spe!4v1582554581694!5m2!1ses-419!2spe" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- FAQ -->
                <div class="row pt3 contacto_faq">
                    <div class="col-xs-12">
                        <h2 class="title-contacto">PREGUNTAS FRECUENTES</h2>
                    </div>

                    <div class="col-xs-12">
                        <ul class="accordion">
                            <li class="wow fadeInDown" data-wow-delay="0s">
                                <h4 class="inactive default">
                                    <span>¿Cuál es el horario de atención de la clínica?</span>
                                    <div class="glyph-wrapper glyph-wrapper_color">
                                        <span class="line-h"></span>
                                        <span class="line-v"></span>
                                    </div>
                                </h4>
                                <div class="accordion-panel">
                                    <div class="row accordion-panel_padding">
                                        <div class="col-xs-12 col-md-4">
                                            Lunes a Viernes:
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            | 8:00 am - 10:00 pm Sábados:
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            8:00 am - 3:00 pm
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="wow fadeInDown" data-wow-delay="0.2s">
                                <h4 class="inactive">
                                    <span>¿Se puede reservar cita por internet o por teléfono?</span>
                                    <div class="glyph-wrapper">
                                        <span class="line-h"></span>
                                        <span class="line-v"></span>
                                    </div>
                                </h4>
                                <div class="accordion-panel">
                                    <div class="row accordion-panel_padding">
                                        <div class="col-xs-12">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum doloremque tempora veniam dolorum consectetur? Perferendis ipsum eaque laborum dicta possimus eius atque nam, quod, impedit est consequatur nulla! Magni, saepe!</p>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="wow fadeInDown" data-wow-delay="0.4s">
                                <h4 class="inactive">
                                    <span>¿Cómo me integro al voluntariado?</span>
                                    <div class="glyph-wrapper">
                                        <span class="line-h"></span>
                                        <span class="line-v"></span>
                                    </div>      
                                </h4>
                                <div class="accordion-panel">
                                    <div class="row accordion-panel_padding">
                                        <div class="col-xs-12">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum doloremque tempora veniam dolorum consectetur? Perferendis ipsum eaque laborum dicta possimus eius atque nam, quod, impedit est consequatur nulla! Magni, saepe!</p>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="wow fadeInDown" data-wow-delay="0.6s">
                                <h4 class="inactive">
                                    <span>¿Cuánto cuesta una cita?</span>
                                    <div class="glyph-wrapper">
                                        <span class="line-h"></span>
                                        <span class="line-v"></span>
                                    </div>      
                                </h4>
                                <div class="accordion-panel">
                                    <div class="row accordion-panel_padding">
                                        <div class="col-xs-12">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum doloremque tempora veniam dolorum consectetur? Perferendis ipsum eaque laborum dicta possimus eius atque nam, quod, impedit est consequatur nulla! Magni, saepe!</p>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="wow fadeInDown" data-wow-delay="0.8s">
                                <h4 class="inactive">
                                    <span>¿Brindamos servicio de resonancia magnetica?</span>
                                    <div class="glyph-wrapper">
                                        <span class="line-h"></span>
                                        <span class="line-v"></span>
                                    </div>      
                                </h4>
                                <div class="accordion-panel">
                                    <div class="row accordion-panel_padding">
                                        <div class="col-xs-12">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum doloremque tempora veniam dolorum consectetur? Perferendis ipsum eaque laborum dicta possimus eius atque nam, quod, impedit est consequatur nulla! Magni, saepe!</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/footer.php'
        ?>
        <script src="assets/js/accordion.js"></script>
    </main>

    <?php
        include 'src/includes/cierre.php'
    ?>