<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/uneteAnosotros.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">PASTORAL DE LA SALUD</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                dimensión</span><br>profética</h2>
                                    </div>
                                    <!-- COLABORADORES -->
                                    <div class="row dflex pb5 dm-colaboradores">
                                        <div class="col-xs-12 col-md-7">
                                            <img src="assets/images/internas/pastoral/dimension-profetica.jpg" alt="Dimensión Profética" class="w-100 img-colaboradores">
                                        </div>

                                        <div class="col-xs-12 col-md-5">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Colaboradores</h2>
                                                        <h3 class="block_h3">Acciones de evangelización</h3>
                                                        <p class="text-internas text-justify">Generado para colaboradores que ingresaron y por diversos motivos no se quedaron en la institución.</p>
                                                        <h3 class="block_h3">Difusiones del evangelio</h3>
                                                        <p class="text-internas text-justify">Envío del evangelio diario por correo a colaboradores, con imágenes alusivas y de reflexión que se hace extensivo a 250 colaboradores y es replicado en servicios asistenciales.</p>
                                                        <h3 class="block_h3">Charlas de reflexión</h3>
                                                        <p class="text-internas text-justify">se iniciaron en septiembre 2017. Realizándose 11 reuniones en septiembre, 18 reuniones en octubre y 12 en noviembre. Participando 127 colaboradores el primer mes, 136 durante el segundo mes y 129 colaboradores el tercer mes.</p>
                                                        <h3 class="block_h3">Marcha por la vida</h3>
                                                        <p class="text-internas text-justify">Participación en el 10°, 11° y 12° Corso por la Vida, donde asistieron colaboradores del centro acompañado de sus hijos y familiares.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PACIENTES ASISTIDOS -->
                                    <div class="col-xs-12">
                                        <h2 class="block_h2 tabfun3">Pacientes asistidos</h2>
                                    </div>
                                    <div class="wrapper-card-repeat row pb5">
                                        <div class="col-xs-12 col-md-4 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos1.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Catequesis</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos1.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Catequesis</p>
                                                        <p class="text-p2">Realizada los sábados hasta abril y todoss domingos desde mayo 2017. Gracias al servicio de voluntariado quienes arman un plan anual y temática.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-4 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos2.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Acciones de Evangelización</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos2.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Acciones de Evangelización</p>
                                                        <p class="text-p2">Realizada los sábados hasta abril y todoss domingos desde mayo 2017. Gracias al servicio de voluntariado quienes arman un plan anual y temática.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-4 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos3.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Difusión del Evangelización</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos3.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Difusión del Evangelización</p>
                                                        <p class="text-p2">Realizada los sábados hasta abril y todoss domingos desde mayo 2017. Gracias al servicio de voluntariado quienes arman un plan anual y temática.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ACCIONES DE EVANGELIZACION -->
                                    <div class="row dflex pb5 acciones-evangelizacion">
                                        <div class="col-xs-12 col-md-5">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Acciones <br> de Evangelización <br> y promoción de derechos</h2>
                                                        <h3 class="block_h3">Marcho por la vida</h3>
                                                        <p class="text-internas text-justify">Participación el 10°, 11° y 12° Corso por la Vida y la Familia, donde asistieron de 8 a 10 niños hospitalizados o en tratamiento en nuestro centro acompañados como siempre de personal asistencial que los acompaño dentro de la combi que recorrió la ruta del corso. Los niños también pegaron letras e imágenes en los carteles que realizamos para el Corso el año 2016.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-7">
                                            <img src="assets/images/internas/pastoral/dimension-profetica2.jpg" alt="" class="w-100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-pastoral-salud.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>