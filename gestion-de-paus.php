<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/informacionAlUsuario.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">INFORMACIÓN AL USUARIO</h1>
        </div>
    </section>

    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0 paddCustomMovil">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                gestión</span><br>de paus</h2>
                                    </div>
                                </div>

                                <!-- RECLAMOS FORMATOS -->
                                <div class="row dflex nflex pb7">
                                    <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                                        <div class="dta">
                                            <div class="dtac">
                                                <div class="">
                                                    <p class="p-internas tabfun3">Reclamos</p>
                                                    <p class="text-internas text-justify">Una vez que el reclamo es recibido por la institución, éste será derivado a la Plataforma de Atención al usuario (área de reclamos), donde se contactará al reclamante para informarle su recepción y los plazos de respuesta.</p>
                                                    <p class="text-internas text-justify">La coordinación de reclamos remitirá los antecedentes a las unidades responsables y solicitará el informe correspondiente. Con esta información procederá a dar respuesta al reclamante.</p>
                                                    
                                                    <p class="p-internas tabfun4">Formatos</p>
                                                    <p class="text-internas sinb">Disponibles en las instalaciones de la clínica</p>
                                                    <div class="espec d-flex">
                                                        <p class="text-internas bold flt">Correo: </p>
                                                        <span class="p-internas text-internas flp">clinicasjd.arequipa@sanjuandedios.pe</span>
                                                    </div>
                                                    <button class="btn-primary btn btn-bus2">Solicitar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                                        <img src="assets/images/internas/inf-usuario/ges1.jpg" alt="" class="w-100 pl3 dn-imgMovil">
                                    </div>
                                </div>

                                <!-- PROCESO DEL RECLAMO -->
                                <div class="col-xs-12 lineas-maestras">
                                    <div class="row">
                                        <div class="img-lineas-maestras col-xs-12 col-sm-10 pd-x-0 wow fadeInUp" data-wow-delay="0.25s">
                                            <img class="img-cover" src="assets/images/internas/inf-usuario/ges2.jpg" alt="">
                                        </div>
                                        <div class="col-xs-12 col-md-4 wrapper-slide-lineas-m wow fadeInRight" data-wow-delay="0.75s">
                                            <div class="m-slide">
                                                <h2 class="sub-ttl-flotant color-primary">Proceso<br>del reclamo</h2>
                                                <div class="pr-icon">
                                                    <span class="icon-document color-secondary"></span>
                                                </div>
                                                <div class="slide-lineas-maestras owl-carousel">
                                                    <div class="text-p2">El reclamo se basará de acuerdo a ley sólo los que se encuentren escritos en el formato de reclamos recibidos y firmados por el reclamante debidamente llenado.</div>
                                                    <div class="div text-p2">El reclamo se basará de acuerdo a ley sólo los que se encuentren escritos en el formato de reclamos recibidos y firmados por el reclamante debidamente llenado.</div>
                                                    <div class="div text-p2">El reclamo se basará de acuerdo a ley sólo los que se encuentren escritos en el formato de reclamos recibidos y firmados por el reclamante debidamente llenado.</div>
                                                    <div class="div text-p2">El reclamo se basará de acuerdo a ley sólo los que se encuentren escritos en el formato de reclamos recibidos y firmados por el reclamante debidamente llenado.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-inf-usuario.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>