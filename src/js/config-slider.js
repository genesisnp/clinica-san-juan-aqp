$(document).ready(function() {
    $('.slider-home').owlCarousel({
        //loop:true,
        nav:true,
        items:1,
        dots: true,
        animateOut: 'fadeOut',
        navText: [
            '<span class="icon-arrow-home-right"></span>',
            '<span class="icon-arrow-home-left"></span>'
        ],
        responsive:{
			0:{
				dots: false
			},
			425:{
				dots: false
			},
			576:{
				dots: false
			},
			768:{
				dots: true
			}
        }
    });

    $('.carrousel-news').owlCarousel({
        //loop:true,
        nav:true,
        items:1,
        navText: [
            '<span class="icon-arrow-right"></span>',
            '<span class="icon-arrow-left"></span>'
        ],
    })

})

const addZeroToIndex = index => index > 0 && index < 10 ? '0' + index : index;
const mainConfig = {
    onInitialized(event) {
        var index = 0;
        var totalPages = event.item.count % event.page.size === 0 ? event.item.count / event.page.size : Math.floor(event.item.count / event.page.size) + 1;
        totalPages = addZeroToIndex(totalPages);

        var buildPagination = function (index, count) {
            return $('<div class="owl-pages"><span class="owl-pages-current">' + function () { return addZeroToIndex(index + 1); }() + '</span>&nbsp;&nbsp;<span class="owl-pages-count">' + count + '</span></div>');
        };
        $(event.target).find('.owl-nav .owl-prev').after(buildPagination(index, totalPages));
    },
    onChanged(event) {
        const newIndex = addZeroToIndex(event.page.index + 1);
        $(event.target).find('.owl-pages-current').text(newIndex);
    },
    onResized(event) {
        $(event.target).find('.owl-pages-current').text(addZeroToIndex(event.page.index + 1));
        $(event.target).find('.owl-pages-count').text(addZeroToIndex(event.item.count % event.page.size === 0 ? event.item.count / event.page.size : Math.floor(event.item.count / event.page.size) + 1));
    }
};

$(document).ready(function() {

    var setOwlCarousel = function (loop, margin, itemsForMobile, itemsForPad, itemForPC) {
        return {
            loop: loop || false,
            //margin: margin || 20,
            nav: true,
            // dots: false,
            //arrows:true,
            navText: [
                '<button class="btn-arrow-right"><span class="icon-arrow-right"></span></button>',
                '<button class="btn-arrow-left"><span class="icon-arrow-left"></span></button>'
            ],
            navElement: 'div',
            responsive: {
                0: { items: itemsForMobile || 1, slideBy: itemsForMobile || 1 },
                768: { items: itemsForPad || 2, slideBy: itemsForPad || 2 },
                992: { items: itemForPC || 3, slideBy: itemForPC || 3 }
            },
            ...mainConfig
        }
    };
    
    $('#slide-beginning').owlCarousel(setOwlCarousel());
});

$('.slide-lineas-maestras').owlCarousel({
    loop:false,
    nav:true,
    items:1,
    navText: [
        '<button class="btn-arrow-right"><span class="icon-arrow-right"></span></button>',
        '<button class="btn-arrow-left"><span class="icon-arrow-left"></span></button>'
    ],
    ...mainConfig
})
/*
$(".slider-servicios").owlCarousel({
    loop: false,
    nav: true,
    items: 1,
    navText: [
        '<button class="btn-arrow-right"><span class="icon-arrow-right"></span></button>',
        '<button class="btn-arrow-left"><span class="icon-arrow-left"></span></button>'
    ],
    ...mainConfig
});*/