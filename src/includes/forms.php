<section class="sct-form">
    <img src="assets/images/bg-sctFormH.jpg" alt="">
    <div class="container">
        <div class="row">
            <!-- DATOS DE FORMULARIO (CLINICA) -->
            <div class="col-xs-12 col-md-6 info-form">
                <div class="row">
                    <div class="col-xs-12">
                        <img class="audi-cont" src="assets/images/icons/audi.svg" alt="">
                        <h1 class="titles-big color-primary">Contáctanos</h1>
                    </div>
                    <p class="col-xs-12 p-internas text-intr-form">Ante cualquier duda que usted presente, estaremos siempre para atenderlo y guiarlo.<br>Envíenos sus dudas mediante el formulario completando los campos solicitados.</p>
                    
                    <div class="phones-info-form">
                        <a href="#" class="quotesCitas phoneInfoForm"><i class="icon-phone"></i><span class="internas-bold ">(+511) 319 1401</span></a>
                        <a href="#" class="quotesInformes phoneInfoForm"><i class="icon-phone"></i><span class="internas-bold ">(+511) 319 1401</span></a>
                    </div>
                    <ul class="fa-ul">
                        <li class="p-internas"><span class="fa-li"><i class="icon-form icon-ubication"></i></span><p class="p-internas">Av.Nicólas Arriola #3250 San Luis, Lima - Perú</p></li>
                        <li class="p-internas"><span class="fa-li"><i class="icon-form icon-sobre"></i></span><p class="p-internas">comunicaciones@sanjuandediosoh.com</p></li>
                    </ul>
                    <!-- REDES SOCIALES -->
                    <div class="col-xs-12 rs-form">
                        <h2 class="ttl-sg titles-big color-primary">Síguenos:</h2>
                        <div class="icons-rs-form">
                            <div class="content-icon-form">
                                <a href="#" class="icons-form-rs icon-face"></a>
                            </div>
                            <div class="content-icon-form">
                                <a href="#" class="icons-form-rs icon-youtube"></a>
                            </div>
                            <div class="content-icon-form">
                                <a href="#" class="icons-form-rs icon-instagram"></a>
                            </div>
                            <div class="content-icon-form">
                                <a href="#" class="icons-form-rs icon-twitter"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FORMULARIO -->
            <div class="col-xs-12 col-md-6 form-container">
                <form action="#" class="form row" method="post" id="form-contact">
                    <div class="form__wrapper col-xs-12">
                        <input type="text" class="form__input" id="name" name="name">
                        <label class="form__label">
                            <span class="form__label-content">Nombres:</span>
                        </label>
                    </div>
                    <div class="form__wrapper col-xs-12">
                        <input type="text" class="form__input" id="lastname" name="lastname">
                        <label class="form__label">
                            <span class="form__label-content">Apellidos:</span>
                        </label>
                    </div>
                    <div class="form__wrapper col-xs-12 col-lg-6">
                        <input type="text" class="form__input" id="phone" name="phone">
                        <label class="form__label">
                            <span class="form__label-content">Teléfono:</span>
                        </label>
                    </div>
                    <div class="form__wrapper col-xs-12 col-lg-6">
                        <input type="email" class="form__input" id="email" name="email">
                        <label class="form__label">
                            <span class="form__label-content">Email:</span>
                        </label>
                    </div>
                    <div class="form__wrapper col-xs-12">
                        <textarea class="form__input form_textarea" id="textarea" name="textarea"></textarea>
                        <label class="form__label">
                            <span class="form__label-content">Mensaje:</span>
                        </label>
                    </div>
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label class="font-titles-md label-pol">
                                <input type="checkbox" /><i class="helper"></i><span>Usted reconoce haber leído
                                    y
                                    aceptado <span class="span-pol internas-bold color-primary btn-modals">la
                                        Política de datos
                                        personales.</span></span>
                            </label>
                        </div>
                        <div class="btn-container">
                            <button type="submit" name="submit" class="btn internas-bold btn-send btn-redHover shine" id="btn-send-form">ENVIAR</button>
                        </div>
                    </div>
                </form>
                <div class="col-xs-12">
                    <div class="wrapper-suscription">
                        <input type="text" class="input-suscription p-internas">
                        <button class="btn-suscription internas-bold">Suscribirse</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>