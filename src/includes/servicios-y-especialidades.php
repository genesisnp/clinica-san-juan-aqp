<div class="tabs text-uppercase">
    <span class="p-internas tab-link current" data-tab="a">A</span>
    <span class="p-internas tab-link" data-tab="b">B</span>
    <span class="p-internas tab-link" data-tab="c">C</span>
    <span class="p-internas tab-link" data-tab="d">D</span>
    <span class="p-internas tab-link" data-tab="e">E</span>
    <span class="p-internas tab-link" data-tab="f">F</span>
    <span class="p-internas tab-link" data-tab="g">G</span>
    <span class="p-internas tab-link" data-tab="h">H</span>
    <span class="p-internas tab-link" data-tab="i">I</span>
    <span class="p-internas tab-link" data-tab="j">J</span>
    <span class="p-internas tab-link" data-tab="k">K</span>
    <span class="p-internas tab-link" data-tab="l">L</span>
    <span class="p-internas tab-link" data-tab="m">M</span>
    <span class="p-internas tab-link" data-tab="n">N</span>
    <span class="p-internas tab-link" data-tab="o">O</span>
    <span class="p-internas tab-link" data-tab="p">P</span>
    <span class="p-internas tab-link" data-tab="q">Q</span>
    <span class="p-internas tab-link" data-tab="r">R</span>
    <span class="p-internas tab-link" data-tab="s">S</span>
    <span class="p-internas tab-link" data-tab="t">T</span>
    <span class="p-internas tab-link" data-tab="u">U</span>
    <span class="p-internas tab-link" data-tab="v">V</span>
    <span class="p-internas tab-link" data-tab="w">W</span>
    <span class="p-internas tab-link" data-tab="x">X</span>
    <span class="p-internas tab-link" data-tab="y">Y</span>
    <span class="p-internas tab-link" data-tab="z">Z</span>
</div>

<div class="content slider-servicios">
    <div class="row borbg" onclick="window.location='detalle-especialidad.php'">        
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s1"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t1">
                <h3 class="azul p-internas colorPrimary mayus disin">CONSULTA EXTERNA</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 30 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div> 
        </div>                                         
    </div>
    <div class="row borbg">
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s2"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t2">
                <h3 class="azul p-internas colorPrimary mayus disin">rehabilitación integral</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 10 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                        
    </div>
    <div class="row borbg">
        <div class="dtgt">
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s3"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t3">
                <h3 class="azul p-internas colorPrimary mayus disin">hospitalización</h3><br>
                <p class="p-internas fs17 sinb disin">Hospitalización para adultos / Hospitalización pediátrica</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    <div class="row borbg">
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s4"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t4">
                <h3 class="azul p-internas colorPrimary mayus disin">emergencia</h3><br>
                <p class="p-internas fs17 sinb disin">6 camas de observación / Unidad de shock trauma</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    <div class="row borbg">
        <div class="dtgt">
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s5"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t5">
                <h3 class="azul p-internas colorPrimary mayus disin">centro quirúrgico</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 5 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    <div class="row borbg">
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s6"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t6">
                <h3 class="azul p-internas colorPrimary mayus disin">uci / ucin</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 7 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    
    <div class="row borbg" onclick="window.location='detalle-especialidad.php'">        
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s1"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t1">
                <h3 class="azul p-internas colorPrimary mayus disin">CONSULTA EXTERNA</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 30 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div> 
        </div>                                         
    </div>
    <div class="row borbg">
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s2"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t2">
                <h3 class="azul p-internas colorPrimary mayus disin">rehabilitación integral</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 10 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                        
    </div>
    <div class="row borbg">
        <div class="dtgt">
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s3"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t3">
                <h3 class="azul p-internas colorPrimary mayus disin">hospitalización</h3><br>
                <p class="p-internas fs17 sinb disin">Hospitalización para adultos / Hospitalización pediátrica</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    <div class="row borbg">
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s4"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t4">
                <h3 class="azul p-internas colorPrimary mayus disin">emergencia</h3><br>
                <p class="p-internas fs17 sinb disin">6 camas de observación / Unidad de shock trauma</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    <div class="row borbg">
        <div class="dtgt">
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s5"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t5">
                <h3 class="azul p-internas colorPrimary mayus disin">centro quirúrgico</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 5 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
    <div class="row borbg">
        <div class="dtgt" >
            <div class="icc-pos op5s"><div class="ico-pos"><span class="fs1em icon-s6"></span></div></div>
            <div class="pl6em op5s distab" data-target=".t6">
                <h3 class="azul p-internas colorPrimary mayus disin">uci / ucin</h3><br>
                <p class="p-internas fs17 sinb disin">Más de 7 especialidades</p><br>
                <a class=""><h5 class="p-internas rojo sinmargin disin">Reservar</h5></a>
            </div>
            <div class="circdiv">
                <div class="circle">
                    <div class="beforec"></div>
                    <div class="afterc"></div>
                </div>
            </div>
        </div>                                             
    </div>
</div>

<div class="tab-content servicios col-md-6 pull-right">
            <div class="tab-pane fade tab-img t1 in">                                            
                <div class="">
                    <img src="assets/images/m1.jpg" alt="" class="sim w-100">
                    <div class="col-md-12 posrel bazul">
                        <div class="posb">
                            <p class=" blanco p-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                    </div>
                </div>                                           
            </div>
            <div class="tab-pane fade tab-img t2 in">
                <div class="">
                    <img src="assets/images/m1.jpg" alt="" class="sim w-100">
                    <div class="col-md-12 posrel bazul">
                        <div class="posb">
                            <p class=" blanco p-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade tab-img t3 in">
                <div class="">
                    <img src="assets/images/m1.jpg" alt="" class="sim w-100">
                    <div class="col-md-12 posrel bazul">
                        <div class="posb">
                            <p class=" blanco p-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade tab-img t4 in">
                <div class="">
                    <img src="assets/images/m1.jpg" alt="" class="sim w-100">
                    <div class="col-md-12 posrel bazul">
                        <div class="posb">
                            <p class=" blanco p-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade tab-img t5 in">
                <div class="">
                    <img src="assets/images/m1.jpg" alt="" class="sim w-100">
                    <div class="col-md-12 posrel bazul">
                        <div class="posb">
                            <p class=" blanco p-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade tab-img t6 in">
                <div class="">
                    <img src="assets/images/m1.jpg" alt="" class="sim w-100">
                    <div class="col-md-12 posrel bazul">
                        <div class="posb">
                            <p class=" blanco p-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                    </div>
                </div> 
            </div>
        </div>