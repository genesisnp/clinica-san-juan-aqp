<div class="wrapper-footer section fp-auto-height">
    <footer class="pos-rel">
        <div class="infoMobile d-flex flex-column text-center">
            <h1 class="csjdF-title text-uppercase titlesBig hidden-lg">clínica san juan de dios arequipa</h1>
            <a href="#" class="csjd-linkF text-internas hidden-lg">(+511) 319 1401</a>
            <a href="#" class="csjd-linkF text-internas hidden-lg">info@csjd.com</a>
            <a href="#" class="csjd-linkF text-internas hidden-lg">Av. Ejército 1020 - Cayma / Provincia de Arequipa</a>
        </div>

        <div class="container pd-x-0">
            <div class="row d-flex jContentB">
                <!-- <ul class="menuF-list">
                    <li class="menuF-item"><a href="#" class="menuF-link text-uppercase">SERVICIOS</a></li>
                    <li class="menuF-item"><a href="#" class="menuF-link text-uppercase">STAFF MÉDICO</a></li>
                    <li class="menuF-item"><a href="#" class="menuF-link text-uppercase">vocaciones</a></li>
                    <li class="menuF-item"><a href="#" class="menuF-link text-uppercase">contacto</a></li>
                </ul> -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><a href="servicios-y-especialidades.php" class="menuF-link text-uppercase">SERVICIOS</a></li>
                    <li class="menuF-item"><a href="staff-medico.php" class="menuF-link text-uppercase">STAFF MÉDICO</a></li>
                    <li class="menuF-item"><a href="contacto.php" class="menuF-link text-uppercase">contacto</a></li>
                </ul>
                <!-- La Clínica -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">La Clínica</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="nuestro-fundador.php" class="submenuF-link">Nuestro fundador</a></li>
                            <li class="submenuF-item"><a href="historia-de-la-oh.php" class="submenuF-link">Historia de la OH</a></li>
                            <li class="submenuF-item"><a href="mision-y-vision.php" class="submenuF-link">Misión y Visión</a></li>
                            <li class="submenuF-item"><a href="principios-y-valores.php" class="submenuF-link">Principios y Valores</a></li>
                            <li class="submenuF-item"><a href="estructura-organica.php" class="submenuF-link">Estructura Orgánica</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Directorio</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Documentos</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Vocaciones</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Información al Usuario -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">Información al usuario</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="convenios.php" class="submenuF-link">Convenios</a></li>
                            <li class="submenuF-item"><a href="deberes-y-derechos.php" class="submenuF-link">Deberes y Derechos</a></li>
                            <li class="submenuF-item"><a href="guia-de-hospitalizacion.php" class="submenuF-link">Guía de Hospitalización</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Procedimiento de Gestión</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Programa de Seguridad</a></li>
                            <li class="submenuF-item"><a href="tips-de-salud.php" class="submenuF-link">Tips de Salud</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Responsabilidad Social -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">Responsabilidad Social</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Mensajeros de la Salud</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Mensajeros de la Noche</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Otras acciones sociales</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Pastoral -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">Pastoral</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Dimensión Profética</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Dimensión Litúrgica</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Dimensión Caritativa</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Dimensión Hospitalaria</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Novedades -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">Novedades</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Revista Digital</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Publicaciones de la OH</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Testimonios</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Únete a Nosotros -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">Únete a Nosotros</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Bolsa Laboral</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Ser Voluntario</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Ser Bienhechor</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Red Hospitalaria -->
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item">
                        <a class="menuF-link text-uppercase">Red Hospitalaria</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Perú</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Venezuela</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Ecuador</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">El Mundo</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <div class="socialNetworksF">
            <a href="#" class="iconF"><span class="icon-face"></span></a>
            <a href="#" class="iconF"><span class="icon-youtube"></span></a>
            <a href="#" class="iconF"><span class="icon-instagram"></span></a>
            <a href="#" class="iconF"><span class="icon-twitter"></span></a>
            <a href="#" class="iconF"><span class="icon-linkedin"></span></a>
        </div>

        <div class="credits">
            <div class="content-info-credits">
                <div>
                    <p class="mb-0"><a class="link-data d-none d-md-block" href="#">CLÍNICA SAN JUAN DE DIOS • AREQUIPA</a></p>
                    <p class="mb-0">
                        <span class="text-data">TODOS LOS DERECHOS RESERVADOS 2019</span>
                    </p>
                </div>
                <div class="content-htcss text-right">
                    <p class="mb-0">
                        <span class="text-data">POWERED BY </span>
                        <a class="link-data" href="http://exe.pe/">EXE.PE</a>
                    </p>
                    <p class="mb-0 hidden-xs hidden-sm">
                        <a class="link-data" href="https://validator.w3.org/check?uri=referer" target="_blank">HTML</a>
                        <span class="text-data">•</span>
                        <a class="link-data" href="https://jigsaw.w3.org/css-validator/check/referer" target="_blank">CSS</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="assets/js/app.js"></script>
<script src="assets/js/libraries/wow.min.js"></script>
<script>
    var wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
</script>