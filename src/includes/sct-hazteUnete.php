<section class="sct-participate">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 flex div-participate">
                <div class="icon-container float-left">
                    <i class="icon-prtcp icon-hazte-voluntario"></i>
                </div>
                <div class="titles-participate float-left">
                    <h1 class="internas-bold">Hazte voluntario</h1>
                    <p class="p-internas">Participa activamente de nuestros programas sociales</p>
                    <div class="content-btn-participate">
                        <a href="#" class="btn btn-participate p-internas-bold">PARTICIPA</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 flex div-participate">
                <div class="icon-container float-left">
                    <i class="icon-prtcp icon-hazte-bienhechor"></i>
                </div>
                <div class="titles-participate float-left">
                    <h1 class="internas-bold">Hazte bienhechor</h1>
                    <p class="p-internas">Ayúdanos económica, material y/o espiritualmente</p>
                    <div class="content-btn-participate">
                        <a href="#" class="btn btn-participate p-internas-bold">PARTICIPA</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 flex div-participate">
                <div class="icon-container float-left">
                    <i class="icon-prtcp icon-hazte-hermano"></i>
                </div>
                <div class="titles-participate float-left">
                    <h1 class="internas-bold">Hazte hermano de San Juan de Dios</h1>
                    <p class="p-internas">Forma parte de nuestra gran familia</p>
                    <div class="content-btn-participate">
                        <a href="#" class="btn btn-participate p-internas-bold">PARTICIPA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>