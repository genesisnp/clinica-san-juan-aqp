<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="assets/images/logos/LOGOPE.ico" rel="shortcut icon" />
    <title>CLÍNICA SAN JUAN DE DIOS AREQUIPA</title>
    <link rel="stylesheet" href="assets/css/app.css">
</head>

<body>
    <?php 
        $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));    
    ?>
    <div class="wrapper-header pos-fix" id="wrapper-header">
        <header class="header container pos-rel">
            <div class="contentLogo">
                <a href="index.php"><img class="logoAqpB" src="assets/images/logos/logoAqpB.png" alt=""></a>
                <a href="index.php"><img class="logoAqp-hover" src="assets/images/logos/logoAqpC.png" alt=""></a>
            </div>
            <div class="content-menuPrincipal">
                <div class="phones-and-socialH">
                    <a href="#" class="d-inBlock font-semi-bold red-hosp btn-redHover shine"><i class="icon-citas"></i>CITAS EN LÍNEA</a>
                    <a href="#" class="d-inBlock quotes phoneInfoForm quoteCitas">
                        <i class="icon-phone"></i>
                        <span class="internas-bold ">(+511) 319 1401</span>
                    </a>
                    <a href="#" class="d-inBlock quotes phoneInfoForm quoteInformes">
                        <i class="icon-phone"></i>
                        <span class="internas-bold ">(+511) 319 1401</span>
                    </a>
                    <div class="container-socialH d-inBlock dNone1024">
                    <a href="#" class="icon-socialH"><span class="icon-face"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-youtube"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-instagram"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-twitter"></span></a>
                    </div>
                </div>
                <ul class="navbarList">
                    <li class="navbarItem dNone1024 <?= in_array('servicios-y-especialidades.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink" href="servicios-y-especialidades.php"><i class="icon-hojasLeft"></i> SERVICIOS <i class="icon-hojasRight"></i></a></li>
                    <li class="navbarItem dNone1024 <?= in_array('staff-medico.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink" href="staff-medico.php"><i class="icon-hojasLeft"></i> STAFF MÉDICO <i class="icon-hojasRight"></i></a></li>
                    <li class="navbarItem dNone1024 <?= (in_array('convenios.php', $uriSegments ) 
                                                        or in_array('deberes-y-derechos.php', $uriSegments )
                                                        or in_array('guia-de-hospitalizacion.php', $uriSegments )
                                                        or in_array('gestion-de-paus.php', $uriSegments )
                                                        or in_array('seguridad-al-paciente.php', $uriSegments )
                                                        or in_array('tips-de-salud.php', $uriSegments ))
                                                        ? 'active' : ''; ?>"><a class="navbarLink" href="convenios.php"><i class="icon-hojasLeft"></i> INFORMACIÓN AL USUARIO <i class="icon-hojasRight"></i></a></li>
                    <li class="navbarItem dNone1024 <?= in_array('responsabilidad-social.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink" href="responsabilidad-social.php"><i class="icon-hojasLeft"></i> RESPONSABILIDAD SOCIAL <i class="icon-hojasRight"></i></a></li>
                    <li class="navbarItem dNone1024 <?= in_array('contacto.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink" href="contacto.php"><i class="icon-hojasLeft"></i> CONTÁCTENOS <i class="icon-hojasRight"></i></a></li>
                </ul>
            </div>
        </header>

        <div id="menuArea">
            <input type="checkbox" id="menuToggle"></input>

            <label for="menuToggle" class="menuOpen">
                <div class="open"></div>
            </label>

            <div class="menu menuEffects">
                <label for="menuToggle"></label>
                <div class="menuContent">
                    <nav class="navigation__nav">
                        <ul class="navigation__list container-fluid pd-x-0">
                            <div class="nav-left col-xs-12 col-md-4 col-lg-4 pd-x-0">
                                <a href="index.php"><img class="logoAqp-hoverMenu" src="assets/images/logos/logoAqpC.png" alt=""></a>
                                <div class="hoverMenu-link">
                                    <a href="servicios-y-especialidades.php" class="ttl-nav-left color-primary">SERVICIOS</a>
                                </div>
                                <div class="hoverMenu-link">
                                    <a href="staff-medico.php" class="ttl-nav-left color-primary">STAFF MÉDICO</a>
                                </div>
                                <div class="m-btn"><a href="#" class="btn-r-hosp font-semi-bold btn-redHover shine"><i class="icon-citas"></i> <span>CITAS EN LÍNEA</span></a></div>
                                <div class="rs-nav-left">
                                    <a href="#" class="icon-nav-left icon-facebook-single"></a>
                                    <a href="#" class="icon-nav-left icon-youtube-single"></a>
                                    <a href="#" class="icon-nav-left icon-instagram-single"></a>
                                    <a href="#" class="icon-nav-left icon-twitter-single"></a>
                                    <a href="#" class="icon-nav-left icon-linkedin-single"></a>
                                </div>
                                <div class="phone-nav-left">
                                    <a href="#" class="color-primary"><span class="internas-bold ">(+511) 319 - 1401</span></a>
                                </div>
                                <p class="text-light-left mb-0">Av. Nicolás Arriola #3250 San Luis, Lima - Perú</p>
                                <a href="#" class="text-light-left">comunicaciones@sanjuandediosoh.com</a>
                            </div>

                            <div class="col-xs-12 col-md-8 col-lg-8">
                                <div class="wrapper-nav-big row">
                                    <ul class="nav-big col-xs-12 col-md-6">
                                        <!-- La Clínica -->
                                        <li class="item-nav-big">
                                            <span class="link-nav-big internas-bold">
                                                <i class="icon-hojas icon-hojasLeft"></i> La Clínica <i class="icon-hojas icon-hojasRight"></i>
                                            </span>
                                            <ul class="subm-nav-big">
                                                <li class="item-subm-big">
                                                    <a href="nuestro-fundador.php" class="link-subm-big">Nuestro fundador</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="historia-de-la-oh.php" class="link-subm-big">Historia de la OH</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="mision-y-vision.php" class="link-subm-big">Misión y visión</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="principios-y-valores.php" class="link-subm-big">Principios y valores</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="estructura-organica.php" class="link-subm-big">Estructura Orgánica</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="nuestro-directorio.php" class="link-subm-big">Nuestro Directorio</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="vocaciones-hospitalarias.php" class="link-subm-big">Vocaciones hospitalarias</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- Información al usuario -->
                                        <li class="item-nav-big">
                                            <span class="link-nav-big internas-bold">
                                                <i class="icon-hojas icon-hojasLeft"></i> Información al usuario <i class="icon-hojas icon-hojasRight"></i>
                                            </span>
                                            <ul class="subm-nav-big">
                                                <li class="item-subm-big">
                                                    <a href="convenios.php" class="link-subm-big">Convenios</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="deberes-y-derechos.php" class="link-subm-big">Deberes y derechos</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="guia-de-hospitalizacion.php" class="link-subm-big">Guía de hospitalización</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="gestion-de-paus.php" class="link-subm-big">Gestión de Paus</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="seguridad-al-paciente.php" class="link-subm-big">Seguridad al paciente</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="tips-de-salud.php" class="link-subm-big">Tips de salud</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- Responsabilidad social -->
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="responsabilidad-social.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> Responsabilidad social <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <!-- Pastoral de la salud -->
                                        <li class="item-nav-big">
                                            <span class="link-nav-big internas-bold">
                                                <i class="icon-hojas icon-hojasLeft"></i> Pastoral de la salud <i class="icon-hojas icon-hojasRight"></i>
                                            </span>
                                            <ul class="subm-nav-big">
                                                <li class="item-subm-big">
                                                    <a href="dimension-profetica.php" class="link-subm-big">Dimensión profética</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="dimension-liturgica.php" class="link-subm-big">Dimension litúrgica</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="dimension-caritativa.php" class="link-subm-big">Dimensión caritativa</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="dimension-hospitalaria.php" class="link-subm-big">Dimensión hospitalaria</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- Novedades -->
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="#">
                                                <i class="icon-hojas icon-hojasLeft"></i> Novedades <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <!-- Únete a nosotros -->
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="unete-a-nosotros.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> Únete a nosotros <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <!-- Red hospitalaria -->
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="red-hospitalaria.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> Red hospitalaria <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <!-- Contacto -->
                                        <li class="item-nav-big contc">
                                            <a class="link-nav-big internas-bold" href="contacto.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> Contacto <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content-imgs col-xs-6">
                                        <div class="img-oh-big img-header-big">
                                            <img class="img-cover" src="assets/images/header/img-header1.jpg" alt="">
                                        </div>      
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>