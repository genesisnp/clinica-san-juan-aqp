<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-news bgInternas page-news">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ttl-news color-primary text-center titles-big"><h2>NOVEDADES</h2></div>
                
                <div class="col-xs-12 novedades-into">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-4 novedades-item">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/ecuador.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-4 novedades-item">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/venezuela.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-4 novedades-item">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-4 novedades-item">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/peru.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</main>

<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>