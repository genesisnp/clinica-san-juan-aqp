<?php
    include 'src/includes/header.php'
?>
    <main id="my-scrollbar" data-scrollbar>
        <section id="parallax" class="sct-banner scroll">
            <div class="degrade-int"></div>
            <img class="img-banner" src="/assets/images/banner/serv.jpg" alt="">
            <div class="content-title-banner container">
                <h1 class="titleBanner text-uppercase">SERVICIOS Y ESPECIALIDADES</h1>
            </div>
        </section>
        <section class="wrapper-info-theClinic pb5 pt3 bg-white">
            <div class="container">
                <?php
                    include 'src/includes/filtro.php'
                ?>
            </div>
            <div class="container posrel pt4em">
                <h2 class="titles-descrip passer t-5"><span class="span-titlesDescrip">NUESTROS SERVICIOS</span><br>Y ESPECIALIDADES</h2>
                <?php
                    include 'src/includes/servicios-y-especialidades.php'
                ?>
            </div>
        </section>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>

        <script>
		$('.row.borbg .op5s').mouseover(function (e) {     
			//get selected href
			var data2 = $(this).attr('data-target');    
			
			//set all nav tabs to inactive
			$('.row.borbg ').removeClass('active');
			
			//get all nav tabs matching the href and set to active
			$('.row.borbg  .op5s[data-target="'+data2+'"]').addClass('active');

			//active tab
			$('.tab-img').removeClass('active');
			$('.tab-img'+data2).addClass('active');

		});
        $('.row.borbg .op5s').mouseout(function (e) {     
			//get selected href
			var data2 = $(this).attr('data-target');    
			
			//set all nav tabs to inactive
			$('.row.borbg ').removeClass('active');
			
			//get all nav tabs matching the href and set to active
			$('.row.borbg  .op5s[data-target="'+data2+'"]').removeClass('active');

			//active tab
			$('.tab-img').removeClass('active');
			$('.tab-img'+data2).removeClass('active');

		})
    </script>
    <?php
            include 'src/includes/cierre.php'
        ?>