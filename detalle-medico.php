<?php
    include 'src/includes/header.php'
?>
    <main>
        <section id="parallax" class="sct-banner scroll">
            <div class="degrade-int"></div>
            <img class="img-banner" src="/assets/images/banner/staffMedico.jpg" alt="">
            <div class="content-title-banner container">
                <h1 class="titleBanner text-uppercase">staff médico</h1>
            </div>
        </section>

        <!--SECCION STAFF MÉDICO-->
        <section class="wrapper-info-theClinic pt3 pb5 w90-movil bg-white">
            <div class="container">
                <?php
                    include 'src/includes/filtro.php'
                ?>
            </div>
            <div class="container posrel pt4em wow fadeInDown" data-wow-delay="0s">
                <h2 class="titles-descrip passer t-5 singleTitle-movil"><span class="span-titlesDescrip">NUESTROS</span><br>ESPECIALSTAS</h2>
                <div class="tabs text-uppercase">
                    <span class="p-internas tab-link current" data-tab="a">A</span>
                    <span class="p-internas tab-link" data-tab="b">B</span>
                    <span class="p-internas tab-link" data-tab="c">C</span>
                    <span class="p-internas tab-link" data-tab="d">D</span>
                    <span class="p-internas tab-link" data-tab="e">E</span>
                    <span class="p-internas tab-link" data-tab="f">F</span>
                    <span class="p-internas tab-link" data-tab="g">G</span>
                    <span class="p-internas tab-link" data-tab="h">H</span>
                    <span class="p-internas tab-link" data-tab="i">I</span>
                    <span class="p-internas tab-link" data-tab="j">J</span>
                    <span class="p-internas tab-link" data-tab="k">K</span>
                    <span class="p-internas tab-link" data-tab="l">L</span>
                    <span class="p-internas tab-link" data-tab="m">M</span>
                    <span class="p-internas tab-link" data-tab="n">N</span>
                    <span class="p-internas tab-link" data-tab="o">O</span>
                    <span class="p-internas tab-link" data-tab="p">P</span>
                    <span class="p-internas tab-link" data-tab="q">Q</span>
                    <span class="p-internas tab-link" data-tab="r">R</span>
                    <span class="p-internas tab-link" data-tab="s">S</span>
                    <span class="p-internas tab-link" data-tab="t">T</span>
                    <span class="p-internas tab-link" data-tab="u">U</span>
                    <span class="p-internas tab-link" data-tab="v">V</span>
                    <span class="p-internas tab-link" data-tab="w">W</span>
                    <span class="p-internas tab-link" data-tab="x">X</span>
                    <span class="p-internas tab-link" data-tab="y">Y</span>
                    <span class="p-internas tab-link" data-tab="z">Z</span>
                </div>
            </div>
            <div class="container detalle-medico">
                <div class="row pt4em">
                    <div class="col-xs-12 col-md-5 posrel wow fadeInRight" data-wow-delay="0.25s">
                        <span class="icon-medico plm"></span>
                        <h2 class="drme"><span>DR. LUIS ÁNGEL</span><br>CEVALLOS ROJAS </h2>

                        <div class="espec">
                            <h3 class="specialty">ESPECIALIDAD:</h3>
                            <span class="p-internas spanSpecialty">Odontología</span>
                        </div>

                        <p class="text-internas">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                        <div class="espec">
                            <h3 class="specialty">REGISTROS:</h3>
                            <span class="p-internas spanSpecialty">
                                <img src="/assets/images/internas/staff/logo-colegio-medico.png" alt="">
                            </span>
                            <span class="p-internas spanSpecialty">
                                <img src="/assets/images/internas/staff/logo-colegio-medico.png" alt="">
                            </span>
                        </div>

                        <div class="wrapper">
                            <div class="container-calendar">
                                <h3 id="monthAndYear"></h3>
                                
                                <div class="button-container-calendar">
                                    <button id="previous" onclick="previous()">&#8249;</button>
                                    <button id="next" onclick="next()">&#8250;</button>
                                </div>
                                
                                <table class="table-calendar" id="calendar" data-lang="en">
                                    <thead id="thead-month"></thead>
                                    <tbody id="calendar-body"></tbody>
                                </table>
                                
                                <div class="footer-container-calendar">
                                    <label for="month">Jump To: </label>
                                    <select id="month" onchange="jump()">
                                        <option value=0>Jan</option>
                                        <option value=1>Feb</option>
                                        <option value=2>Mar</option>
                                        <option value=3>Apr</option>
                                        <option value=4>May</option>
                                        <option value=5>Jun</option>
                                        <option value=6>Jul</option>
                                        <option value=7>Aug</option>
                                        <option value=8>Sep</option>
                                        <option value=9>Oct</option>
                                        <option value=10>Nov</option>
                                        <option value=11>Dec</option>
                                    </select>
                                    <select id="year" onchange="jump()"></select>       
                                </div>
                            </div>
                        </div>
                        <div class="pt4em"><button class="btn-primary btn btn-bus2">Separa tu cita</button></div>
                    </div>
                    <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.5s">
                        <img src="assets/images/internas/staff/dr1.jpg" class="w-100 detalle-medico_img" alt="">
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>

    <script>
        function generate_year_range(start, end) {
            var years = "";
            for (var year = start; year <= end; year++) {
                years += "<option value='" + year + "'>" + year + "</option>";
            }
            return years;
        }

        today = new Date();
        currentMonth = today.getMonth();
        currentYear = today.getFullYear();
        selectYear = document.getElementById("year");
        selectMonth = document.getElementById("month");


        createYear = generate_year_range(1970, 2050);
        /** or
        * createYear = generate_year_range( 1970, currentYear );
        */

        document.getElementById("year").innerHTML = createYear;

        var calendar = document.getElementById("calendar");
        var lang = calendar.getAttribute('data-lang');

        var months = "";
        var days = "";

        var monthDefault = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];

        var dayDefault = ["L", "M", "M" , "J", "V", "S", "D"];

        if (lang == "en") {
            months = monthDefault;
            days = dayDefault;
        } else if (lang == "id") {
            months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
            days = ["Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"];
        } else if (lang == "fr") {
            months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            days = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"];
        } else {
            months = monthDefault;
            days = dayDefault;
        }

        var $dataHead = "<tr>";
        for (dhead in days) {
            $dataHead += "<th data-days='" + days[dhead] + "'>" + days[dhead] + "</th>";
        }
        $dataHead += "</tr>";

        //alert($dataHead);
        document.getElementById("thead-month").innerHTML = $dataHead;

        monthAndYear = document.getElementById("monthAndYear");
        showCalendar(currentMonth, currentYear);

        function next() {
            currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
            currentMonth = (currentMonth + 1) % 12;
            showCalendar(currentMonth, currentYear);
        }

        function previous() {
            currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
            currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
            showCalendar(currentMonth, currentYear);
        }

        function jump() {
            currentYear = parseInt(selectYear.value);
            currentMonth = parseInt(selectMonth.value);
            showCalendar(currentMonth, currentYear);
        }

        function showCalendar(month, year) {
            var firstDay = ( new Date( year, month ) ).getDay();

            tbl = document.getElementById("calendar-body");
            
            tbl.innerHTML = "";

            monthAndYear.innerHTML = months[month] + " " + year;
            selectYear.value = year;
            selectMonth.value = month;

            // creating all cells
            var date = 1;
            for ( var i = 0; i < 6; i++ ) {
                var row = document.createElement("tr");

                for ( var j = 0; j < 7; j++ ) {
                    if ( i === 0 && j < firstDay ) {
                        cell = document.createElement( "td" );
                        cellText = document.createTextNode("");
                        cell.appendChild(cellText);
                        row.appendChild(cell);
                    } else if (date > daysInMonth(month, year)) {
                        break;
                    } else {
                        cell = document.createElement("td");
                        cell.setAttribute("data-date", date);
                        cell.setAttribute("data-month", month + 1);
                        cell.setAttribute("data-year", year);
                        cell.setAttribute("data-month_name", months[month]);
                        cell.className = "date-picker";
                        cell.innerHTML = "<span>" + date + "</span>";

                        if ( date === today.getDate() && year === today.getFullYear() && month === today.getMonth() ) {
                            cell.className = "date-picker selected";
                        }
                        row.appendChild(cell);
                        date++;
                    }
                }

                tbl.appendChild(row);
            }
        }

        function daysInMonth(iMonth, iYear) {
            return 32 - new Date(iYear, iMonth, 32).getDate();
        }
    </script>

    <?php
        include 'src/includes/cierre.php'
    ?>