<?php
    include 'src/includes/header.php'
?>
<main>
    <!--BANNER-->
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/unete-a-nosotros.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">Únete a nosotros</h1>
        </div>
    </section>

    <section class="sct-joins-us bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 pd-x-0">
                            <h2 class="titles-descrip"><span class="icon-san"></span>
                                <span class="span-titlesDescrip">
                                conociendo</span><br>la orden hospitalaria</h2>
                        </div>
                    </div>
                </div>
                <!-- ETAPAS DE CONOCIMIENTO -->
                <div class="col-xs-12 pd-x-0 description-etaps">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <h2 class="sub-ttl-flotant color-primary">Etapas de conocimiento<br>y discernimiento</h2>
                            <div class="etaps etap-one">
                                <h3 class="ttl-etaps color-primary font-semi-bold">Primera etapa</h3>
                                <p class="text-p2">Se caracteriza por ser un tiempo de “ver”, donde el joven expresa su interés 
                                    de conocer lo que hacemos, por lo cual se le invita a que participe de actividades del 
                                    voluntariado, visitas a alguna comunidad u obra nuestra, retiros juveniles y participación en 
                                    reuniones informativas del carisma hospitalario.</p>
                            </div>
                            <div class="etaps etap-two">
                                <h3 class="ttl-etaps color-primary font-semi-bold">Segunda etapa</h3>
                                <p class="text-p2">La segunda etapa, partimos de la premisa de que el joven ya manifiesta expresamente 
                                    su decisión de comenzar un proceso formal de discernimiento a la Vida Religiosa y a la Orden 
                                    Hospitalaria de San Juan de Dios. En estos casos, el joven es considerado como Aspirante y se le 
                                    propone el Programa de Acompañamiento Vocacional.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
                            <div class="img-etapas">
                                <img class="img-cover" src="assets/images/internas/unete-a-us1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="col-xs-12">
                    <div class="img-two-etaps col-xs-12 col-sm-10 pd-x-0">
                        <img class="img-cover" src="assets/images/internas/unete-a-us2.jpg" alt="">
                    </div>
                    <div class="description-flotant descp-flot-r">
                        <p class="text-p2 text-border">A todo joven que desea profundizar en su 
                            vida cristiana y vocacional le ofrecemos la oportunidad de discernir su 
                            inquietud, clarificando con transparencia sus motivaciones internas, contrastando 
                            sus cualidades y talentos personales con los valores y exigencias propias de la vida 
                            y misión del religioso de San Juan de Dios, para que de esta manera pueda tomar 
                            una decisión responsable.</p>
                    </div>
                </div>
                <!-- TRABAJA CON NOSOTROS -->
                <div class="col-xs-12 pd-x-0">
                    <h2 class="titles-descrip"><span class="icon-san"></span>
                        <span class="span-titlesDescrip">
                        trabaja</span><br>con nosotros</h2>
                </div>
                <div class="col-xs-12 pd-x-0">
                    <div class="row dscrp-trab-us">
                        <div class="col-xs-12 col-md-5">
                            <p class="text-p2">La oficina de Capital Humano de la Provincia Sudamericana Septentrional 
                                brinda la oportunidad de incorporarse a su equipo de profesionales y formar parte de 
                                nuestra bolsa de trabajo enviando su currículum vitae, señalando el centro al que postula 
                                y el puesto que le gustaría desempeña</p>
                            <a href="#"><img src="assets/images/logos/logo-bumeran.png" alt="" class="logo-bumeran"></a>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <div class="wrapper-form-postul">
                                <form action="#" class="form row" method="post" id="form-post">
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input" id="name-postulante" name="name-postulante">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input" id="lastname-postulante" name="lastname-postulante">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input" id="centro" name="centro">
                                        <label class="form__label">
                                            <span class="form__label-content">Centro al postula</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input" id="profesion" name="profesion">
                                        <label class="form__label">
                                            <span class="form__label-content">Profesión</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input" id="pais" name="pais">
                                        <label class="form__label">
                                            <span class="form__label-content">País</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input" id="phone-postulante" name="phone-postulante">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <div class="input_file">
                                            <label class="file_label p-internas">
                                                <span class=""><i class="icon-adjuntar"></i> Adjuntar</span>
                                            </label>
                                            <input id="file" type="file" class="font-titles-md" name="file" multiple />
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12">
                                        <div class="btn-container">
                                            <button type="submit" name="submit" class="btn internas-bold btn-send"
                                                id="btn-send-form">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SCT PARTICIPATE -->
    <?php
        include 'src/includes/sct-hazteUnete.php'
    ?>
</main>

<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>
<script>
    $(document).ready(function () {

        $("#file").on("change", function (e) {
            var files = $(this)[0].files;
            if (files.length >= 2) {
                $(".file_label").text(files.length + " Adjuntar archivo");
            } else {
                var fileName = e.target.value.split("\\").pop();
                $(".file_label").text(fileName);
            }
        });
    });
</script>

</body>

</html>