<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/uneteAnosotros.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">PASTORAL DE LA SALUD</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                dimensión</span><br>hospitalaria</h2>
                                    </div>
                                    <!-- COLABORADORES -->
                                    <div class="row dflex pb5 colaboradores">
                                        <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Colaboradores</h2>
                                                        <h3 class="block_h3">Retiros espirituales</h3>
                                                        <p class="text-internas text-justify">Se han realizado siete fechas durante el 2016, participando 300 colaboradores de las distintas áreas del Centro. Y durante el 2017 no se concretó ninguno por estar en evaluación por la Comunidad de Hermanos y Gerente responsable.</p>
                                                        <h3 class="block_h3">Charla formativas bimensuales</h3>
                                                        <p class="text-internas text-justify">Se han realizado cinco fechas durante el 2016, participando 80 colaboradores nuevos durante ese periodo.</p>
                                                        <h3 class="block_h3">Jornada de reflexión con los voluntarios</h3>
                                                        <p class="text-internas text-justify">Se realizó la Asamblea Anual en marzo 2016 donde participaron 20 voluntarios. Y en total con invitados y voluntarios antiguos 40 en la jornada. En febrero del 2017, se realizó asamblea anual con 25 voluntarios participantes donde se eligió a la coordinación nueva.</p>
                                                        <h3 class="block_h3">Escucha activa</h3>
                                                        <p class="text-internas text-justify">Número colaboradores que se acercan con frecuencia a Pastoral para comunicar su necesidad de consejería espiritual en el momento que lo necesitan.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                                            <img src="assets/images/internas/pastoral/dimension-hospitalaria01.jpg" alt="Dimensión Hospitalaria" class="w-100">
                                        </div>
                                    </div>
                                    <!-- ASISTIDOS -->
                                    <div class="col-xs-12">
                                        <h2 class="block_h2 tabfun3">Asistidos</h2>
                                    </div>
                                    <div class="wrapper-card-repeat row pb5">
                                        <div class="col-xs-12 col-md-3 cols wow zoomIn" data-wow-delay="0.1s" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos1.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Acogida</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos1.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Acogida</p>
                                                        <p class="text-p2">Figuran las bienvenidas realizadas a asistidos por el agente pastoral (2016), equipo de pastoral (2017) y la acogida realizada por la Asistente de Pastoral a madres de recién nacidos en el Centro. Siendo un total 95 bienvenidas a adultos y 507 a recién nacidos realizadas el 2016.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-3 cols wow zoomIn" data-wow-delay="0.3s" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos2.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Visita <br> Pastoral</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos2.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Visita <br> Pastoral</p>
                                                        <p class="text-p2">Realizada los sábados hasta abril y todoss domingos desde mayo 2017. Gracias al servicio de voluntariado quienes arman un plan anual y temática.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-3 cols wow zoomIn" data-wow-delay="0.6s" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos3.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Escucha <br> Activa</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos3.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Escucha <br> Activa</p>
                                                        <p class="text-p2">Realizada los sábados hasta abril y todoss domingos desde mayo 2017. Gracias al servicio de voluntariado quienes arman un plan anual y temática.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-3 cols wow zoomIn" data-wow-delay="0.9s" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos4.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Intervención <br> Pastoral</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos4.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Intervención <br> Pastoral</p>
                                                        <p class="text-p2">Realizada los sábados hasta abril y todoss domingos desde mayo 2017. Gracias al servicio de voluntariado quienes arman un plan anual y temática.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ESPACIO DE FORMACIÓN -->
                                    <div class="row dflex pb5 espacio-formacion">
                                        <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.25s">
                                            <img src="assets/images/internas/pastoral/dimension-hospitalaria02.jpg" alt="Dimensión Hospitalaria" class="w-100">
                                        </div>

                                        <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.75s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Espacio <br> de formación</h2>
                                                        <h3 class="block_h3">Talleres vivenciales 2016</h3>
                                                        <p class="text-internas text-justify">Se programaron en las dos fechas de visita de Carmen Flores, participando en abril 177 colaboradores, y en diciembre 299 colaboradores de los diferentes servicios. En ambas fechas incluimos a los 21 profesores del CRIP SJD. Lo que hace un total de 476 colaboradores.</p>

                                                        <h3 class="block_h3">Talleres vivenciales 2017</h3>
                                                        <p class="text-internas text-justify">Se programaron también dos fechas, participando en marzo 239 colaboradores, y en agosto 298 colaboradores de los diferentes servicios. En ambas fechas incluimos a los 23 profesores del CRIP SJD. Lo que hace un total anual de 527 colaboradores.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- RUTA DE LA HOSPITALIDAD -->
                                    <div class="row dflex ruta-hospitalidad">
                                        <div class="col-xs-12 col-md-6 wow fadeInRight" data-wow-delay="0.25s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Ruta <br> de la hospitalidad</h2>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3">
                                                                <h3 class="title-age">2012</h3>
                                                            </div>
                                                            <div class="col-xs-12 col-md-9">
                                                                <p class="text-internas text-justify">En Arequipa se realizó este programa de vivencia del Carisma y huellas de San Juan de Dios  para 220 colaboradores.</p>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3">
                                                                <h3 class="title-age">2016</h3>
                                                            </div>
                                                            <div class="col-xs-12 col-md-9">
                                                                <p class="text-internas text-justify">Se brindaron 06 salidas participando colaboradores que ingresaron entre el 2014 y 2016.</p>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3">
                                                                <h3 class="title-age">2017</h3>
                                                            </div>
                                                            <div class="col-xs-12 col-md-9">
                                                                <p class="text-internas text-justify">Se realizaron 04 salidas con un total de 53 colaboradores y 01 voluntario participante.</p>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3">
                                                                <h3 class="title-age">2018</h3>
                                                            </div>
                                                            <div class="col-xs-12 col-md-9">
                                                                <p class="text-internas text-justify">Se llevaron a cabo 04 salidas: en enero, marzo, mayo y junio. A la fecha han participado 49 colaboradores.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.75s">
                                            <img src="assets/images/internas/pastoral/dimension-hospitalaria03.jpg" alt="Dimensión Hospitalaria" class="w-100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-pastoral-salud.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>