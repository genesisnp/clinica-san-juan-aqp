<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/informacionAlUsuario.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">INFORMACIÓN AL USUARIO</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0 paddCustomMovil">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                programa de seguridad</span><br>atención al paciente</h2>
                                    </div>
                                </div>
                                <!-- Información del paciente -->
                                <div class="col-xs-12 pb7 informacion-paciente">
                                    <div class="row">
                                        <div class="posrel flex-reverseMovil">
                                            <img src="assets/images/internas/inf-usuario/seg1.jpg" alt="" class="w-100 pl3 noPadImg-sm">
                                            <div class="btab">
                                                <p class="p-internas tabfun3">Información del paciente</p>
                                                <p class="text-internas text-justify sinb">Durante su hospitalización nos preocupamos por el cuidado y la seguridad del paciente para mejorar la calidad de atención de nuestros usuarios, y como parte de la cultura de seguridad al paciente se ha promovido objetivos de calidad de atención.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Identificación de pacientes -->
                                <div class="row dflex flex-reverseMovil pb7 identificacion-pacientes">
                                    <div class="col-xs-12 col-md-7 ">
                                        <img src="assets/images/internas/inf-usuario/seg2.jpg" alt="" class="w-100 pr3 noPadImg-sm">
                                    </div>
                                    <div class="col-xs-12 col-md-5">
                                        <div class="dta">
                                            <div class="dtac">
                                                <div class="">
                                                    <p class="p-internas tabfun3">Identificación<br>de pacientes</p>
                                                    <p class="text-internas text-justify bold">Como parte de la seguridad del paciente es importante el uso de identificadores (brazaletes) de los cuales son los siguientes:</p>
                                                    <ul>
                                                        <li class="text-internas sinb fleli text-justify">Para el caso de identificación de datos como nombres y apellidos completos y su número de identificación, el brazalete será de color blanco. Recuerde que es importante la verificación que los datos estén correctos.</li>
                                                        <li class="text-internas sinb fleli text-justify">Para caídas de riesgo, el paciente utilizará un brazalete de color amarillo para ser identificado.</li>
                                                        <li class="text-internas sinb fleli text-justify">Para el caso de alergias, el paciente será identificado con un brazalete de color rojo. </li>
                                                    </ul> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Mejorar comunicación efectiva -->
                                <div class="row dflex nflex pb7 mejorar-comunicacion">
                                    <div class="col-xs-12 col-md-5">
                                        <div class="dta">
                                            <div class="dtac">
                                                <div class="">
                                                    <p class="p-internas tabfun3">Mejorar comunicación efectiva</p>
                                                    <p class="text-internas text-justify bold">Al momento de su atención, el equipo de salud procederá a darle el soporte y la atención indicada para ello debe de tener en cuenta:</p>
                                                    <ul>
                                                        <li class="text-internas sinb fleli text-justify">El médico le informará sobre su estado de salud y el tratamiento a seguir durante su hospitalización.</li>
                                                        <li class="text-internas sinb fleli text-justify">Su médico dejará en el plan de trabajo la dieta específica a seguir y el personal de nutrición procederá a entregar la alimentación de acuerdo a lo indicado por el médico.</li>
                                                        <li class="text-internas sinb fleli text-justify">Tanto el médico como el personal de enfermería informarán sobre la medicación que recibirá durante su hospitalización.</li>
                                                        <li class="text-internas sinb fleli text-justify">Como paciente es importante informar a su médico tratante y personal de enfermería sobre las alergias, medicamentos, alimentos entre otros del cual usted presenta. </li>
                                                    </ul>                           
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-7 ">
                                        <img src="assets/images/internas/inf-usuario/seg3.jpg" alt="" class="w-100 pl3 noPadImg-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-inf-usuario.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>