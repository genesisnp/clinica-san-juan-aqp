<?php
    include 'src/includes/header.php'
?>
<main>
    <!--BANNER-->
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/laClinica.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">LA CLÍNICA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                nuestra</span><br>misión y visión</h2>
                                    </div>
                                    <div class="col-xs-12 pd-x-0">
                                        <div class="row content-mv pr-3">
                                            <div class="col-xs-12 col-md-8 wrapper-mv">
                                                <div class="img-mv-oh">
                                                    <img class="img-cover" src="assets/images/internas/la-clinica/mision-vision.jpg" alt="">
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-md-4">
                                                <div class="row">
                                                    <div class="mission">
                                                        <h2 class="sub-ttl-flotant color-primary">Misión</h2>
                                                        <p class="text-internas text-justify">Somos una institución de servicio de salud, que pertenece a la orden hospitalaria San Juan de Dios, dedicada a la atención especializada en Medicina y Rehabilitación Integral, siendo nuestro principal objetivo la población vulnerable.</p>
                                                    </div>
                                                    <div class="vission">
                                                        <h2 class="sub-ttl-flotant color-primary">Visión</h2>
                                                        <p class="text-internas text-justify">Ser una institución sanitaria integrada a las redes de salud pública y privada, posicionándose como una institución líder en la región Nor-Oriente del Perú en Ortopedia, Traumatología, Medicina Física y Rehabilitación Integral.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>
</body>

</html>