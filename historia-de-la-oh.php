<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/laClinica.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">LA CLÍNICA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <!--HISTORIA DE LA OH-->
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip">
                                            <span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                historia</span><br>de la orden hospitalaria</h2>
                                    </div>
                                    <!-- NOSOTROS -->
                                    <div class="col-xs-12 padd-0">
                                        <div class="us-oh pr-3">
                                            <div class="col-xs-12 col-md-11 img-us-oh float-right pr-1">
                                                <img class="img-cover" src="assets/images/internas/la-clinica/hist-oh-1.jpg" alt="">
                                            </div>
                                            <div class="description-flotant descp-flot-l">
                                                <h2 class="sub-ttl-flotant color-primary">Nosotros</h2>
                                                <p class="text-p2">La Orden Hospitalaria de San Juan de Dios es una Institución religiosa de confesionalidad 
                                                    católica y sin ánimo de lucro, que promueve la ayuda a necesitados y enfermos en los ámbitos social y 
                                                    sanitario. A través de la Hospitalidad fomentamos una asistencia integral y humanizada guiada por nuestros 
                                                    valores: calidad, respeto, responsabilidad y espiritualidad. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 padd-0">
                                        <div class="us-oh">
                                            <div class="img-us-oh float-left">
                                                <img class="img-cover" src="assets/images/internas/la-clinica/hist-oh-2.jpg" alt="">
                                            </div>
                                            <div class="description-flotant descp-flot-r">
                                                <p class="text-p2 text-border">Contribuimos a generar un desarrollo social más justo. Intervenimos, a través de 
                                                diversos programas socio-sanitarios, en aquellos colectivos que precisan nuestra ayuda. Con la participación de 
                                                Colaboradores y Voluntarios, hacemos llegar nuestros programas de atención integral a todas las partes del mundo y 
                                                formamos una de las mayores organizaciones internacionales de cooperación sin ánimo de lucro del mundo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- CONTRIBUCION -->
                                    <div class="col-xs-12 padd-0">
                                        <div class="contrb-oh row">
                                            <div class="description-contrb col-xs-12 col-md-5">
                                                <h2 class="sub-ttl-flotant color-primary">Contribución</h2>
                                                <p class="text-internas">Nuestra acción se proyecta en Hospitales, Centros Asistenciales, Centros de Salud, Servicios 
                                                Sociales y Comunidades de religiosos. Todos los Centros se caracterizan por su pluralidad y, sobre todo, por un compromiso 
                                                social compartido con muchas otras instituciones de carácter público, eclesial o privado con las que existe una afinidad en 
                                                la motivación y una visión compartida.</p>
                                                <p class="text-internas">Esta predilección por las personas más vulnerables, sea a causa de su enfermedad o por su limitación 
                                                de recursos, nos lleva a la puesta en marcha y desarrollo de programas internacionales de acción social y de salud. </p>
                                            </div>
                                            <div class="content-img-contrb col-xs-12 col-md-7 hidden-xs">
                                                <img src="assets/images/internas/la-clinica/contribucion-oh.jpg" alt="" class="img-cover">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PRESENCIA DE LA ORDEN -->
                                    <div class="col-xs-12 padd-0">
                                        <div class="contrb-oh row">
                                            <div class="content-img-contrb hidden-xs hidden-sm col-md-7">
                                                <img src="assets/images/internas/la-clinica/presencia-oh.jpg" alt="" class="img-cover">
                                            </div>
                                            <div class="description-contrb --presence col-xs-12 col-md-5">
                                                <h2 class="sub-ttl-flotant ttl-presence color-primary">Presencia<br>de la Orden</h2>
                                                <ul>
                                                    <li>
                                                        <h2 class="number-presence color-primary internas-bold">50</h2>
                                                        <h3 class="ttl-sub-presence color-secondary font-semi-bold">países</h3>
                                                        <p class="text-p2">En los que estamos presentes en los 5 continentes.</p>
                                                    </li>
                                                    <li>
                                                        <h2 class="number-presence color-primary internas-bold">400</h2>
                                                        <h3 class="ttl-sub-presence color-secondary font-semi-bold">obras</h3>
                                                        <p class="text-p2">Apostólicas en las cuáles apoyamos a nivel mundial.</p>
                                                    </li>
                                                    <li>
                                                        <h2 class="number-presence color-primary internas-bold">45</h2>
                                                        <h3 class="ttl-sub-presence color-secondary font-semi-bold">+ mil</h3>
                                                        <p class="text-p2">Colaboradores profesionales con una vinculación laboral.</p>
                                                    </li>
                                                    <li>
                                                        <h2 class="number-presence color-primary internas-bold">20</h2>
                                                        <h3 class="ttl-sub-presence color-secondary font-semi-bold">millones</h3>
                                                        <p class="text-p2">De beneficiarios asistidos a nivel mundial hasta el 2018.</p>
                                                    </li>
                                                    <li>
                                                        <h2 class="number-presence color-primary internas-bold">8</h2>
                                                        <h3 class="ttl-sub-presence color-secondary font-semi-bold">mil</h3>
                                                        <p class="text-p2">Voluntarios junto a un ejército de bienhechores que nos dan su apoyo.</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>
</body>

</html>