<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/uneteAnosotros.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">PASTORAL DE LA SALUD</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                dimensión</span><br>litúrgica</h2>
                                    </div>
                                    <!-- CELEBRACIONES LITURGICAS -->
                                    <div class="row dflex pb5 celebraciones-liturgicas">
                                        <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Celebraciones litúrgicas</h2>
                                                        <h3 class="block_h3">Eucaristía celebradas con los niños</h3>
                                                        <p class="text-internas text-justify">Todos los asistidos en Sala de Niños han participado con los Hermanos, sus familiares y colaboradores de turno en la misa dominical. Aproximadamente son 15 en cada sesión en las 48 semanas del 2016.</p>
                                                        <h3 class="block_h3">Eucaristía celebradas con los colaboradores</h3>
                                                        <p class="text-internas text-justify">Una por cada mes del año con participación de 30 colaboradores por fecha durante el 2016 y 2017.</p>
                                                        <h3 class="block_h3">Conmemoración del santoral de la OH</h3>
                                                        <p class="text-internas text-justify">Se han realizado oficialmente 03 celebraciones del Santoral de acuerdo a lo concretado por el Gerente de Pastoral y Consejo Directivo. Asistiendo de manera voluntaria 16 a 17 colaboradores durante el 2017.</p>
                                                        <h3 class="block_h3">Rezo del mediodía</h3>
                                                        <p class="text-internas text-justify">A esta oración del Ángelus / Reina del Cielo se ha agregado la inicio de jornada laboral a las 8am y la del Inmaculado Corazón de Jesús a las 6pm a fines del 2017.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                                            <img src="assets/images/internas/pastoral/dimension-liturgica.jpg" alt="Dimension Litúrgica" class="w-100">
                                        </div>
                                    </div>
                                    <!-- RECURSOS SANANTES -->
                                    <div class="col-xs-12">
                                        <h2 class="block_h2 tabfun3">Recursos sanantes <br> de la oración y sanantes</h2>
                                    </div>
                                    <div class="wrapper-card-repeat row">
                                        <div class="col-xs-12 col-md-6 cols wow zoomIn" data-wow-delay="0.1s" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos2.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Celebración <br> de sacramentos sanantes</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos2.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Celebración de sacramentos sanantes</p>
                                                        <p class="text-p2">En el 2016 se realizaron 394 oraciones anuales; siendo 65 la oración a bebés recién nacidos, 4 post operatorias y 325 por salud.</p>
                                                        <p class="text-p2">Durante el 2017 se oró agradeciendo la llegada de 222 recién nacidos, 10 pre operatorios, 19 post operatorios y 586 mejorías de salud. Totalizando 837 oraciones anuales.</p>
                                                        <p class="text-p2">Este 2018 van: 175 para recién nacidos, 2 post operatorios, 13 pre operatorios, 70 por mejoría de salud. Haciendo un total de 260 oraciones en el primer semestre.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-6 cols wow zoomIn" data-wow-delay="0.25s" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/pastoral/dh_asistidos1.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Oración con y por los asistidos</p>
                                                    </div>
                                                </div>

                                                <div class="back" style="background-image: url(assets/images/internas/pastoral/dh_asistidos1.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Oración con y por los asistidos</p>
                                                        <p class="text-p2">En el 2016 se realizaron 394 oraciones anuales; siendo 65 la oración a bebés recién nacidos, 4 post operatorias y 325 por salud.</p>
                                                        <p class="text-p2">Durante el 2017 se oró agradeciendo la llegada de 222 recién nacidos, 10 pre operatorios, 19 post operatorios y 586 mejorías de salud. Totalizando 837 oraciones anuales.</p>
                                                        <p class="text-p2">Este 2018 van: 175 para recién nacidos, 2 post operatorios, 13 pre operatorios, 70 por mejoría de salud. Haciendo un total de 260 oraciones en el primer semestre.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-pastoral-salud.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>