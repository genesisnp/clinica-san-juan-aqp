<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/uneteAnosotros.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">PASTORAL DE LA SALUD</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                dimensión</span><br>caritativa</h2>
                                    </div>
                                    <!-- MENSAJEROS DE LA SALUD -->
                                    <div class="row dflex pb5 mensajeros-salud">
                                        <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Mensajeros <br> de la salud</h2>
                                                        <p class="text-internas text-justify">En el 2016 se atendió 2219 asistidos en Tuti, 1151 en Coporaque, 2560 en Orurillo, 1247 en Asillo y 1315 en San Antonio de Moquegua.</p>
                                                        <p class="text-internas text-justify">Durante el primer semestre, las misiones 95, 96 y 97 como siempre estuvieron organizadas por la Gerencia de Pastoral. Y la misión N° 98 y 99 a cargo directo de la Gerencias: Médica y Administrativa con apoyo de Recaudación de Fondos. Donde Pastoral ha asesorado y durante el desarrollo de la misión ha colaborado con las coordinaciones de las mismas.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                                            <img src="assets/images/internas/pastoral/pr.jpg" alt="Mensajeros de la salud" class="w-100">
                                        </div>
                                    </div>
                                    <!-- MENSAJEROS DE LA SALUD - CAPTION -->
                                    <div class="col-xs-12 pd-x-0 pb7 wow fadeInUp" data-wow-delay="0s">
                                        <div class="row">
                                            <div class="posrel">
                                                <img src="assets/images/internas/pastoral/mensajeros-salud-02.jpg" alt="" class="w-100 pr3">
                                                <div class="btabr tet2">
                                                    <p class="text-internas text-justify sinb">En el año 2017 este programa fue encargado a la Administración de Fondos y a partir de septiembre a la Gerencia de Responsabilidad Social. Se visitó: Potoni, Moho, Ocoña, Chechén y Coporaque. Este año se ha acompañado las misiones de febrero y abril compartiendo la espiritualidad con la delegación mensajera y población atendida en San Miguel-Juliaca y San Antonio de Moquegua.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- SEDE VOLUNTARIADO -->
                                    <div class="row dflex sede-voluntariado">
                                        <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="dimension_block">
                                                        <h2 class="block_h2 tabfun3">Sede <br> de voluntariado</h2>
                                                        <p class="text-internas text-justify">En la labor interna: acompaña en Sala de Niños, y en cuanto a la labor externa mediante el servicio del Comedor Santa Teresa, atienden a comensales, personas sin techo y familia que reciben alimento. Ayudan a servir el almuerzo y acompañan a estos asistidos en actividades como Viernes Santo, día de la madre, día del padre, agasajo de “no cumpleaños”, navidad entre otros.</p>
                                                        <p class="text-internas text-justify">Los asistidos en Comedor son de 25 a 30 por semana, algunos antiguos ya no han continuado y han aparecido nuevos por ello se ha colocado a 55 durante el 2016. Durante el 2017 se acompañó de 30 a 40 asistidos en el año.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                                            <img src="assets/images/internas/pastoral/sede-voluntariado.jpg" alt="Sede de voluntariado" class="w-100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-pastoral-salud.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>