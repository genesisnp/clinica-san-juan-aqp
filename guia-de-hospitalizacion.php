<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="/assets/images/banner/informacionAlUsuario.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">INFORMACIÓN AL USUARIO</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0 paddCustomMovil">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                guía</span><br>de hospitalización</h2>
                                    </div>
                                </div>

                                <!-- SERVICIO -->
                                <div class="row dflex nflex pb7">
                                    <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                                        <div class="dta">
                                            <div class="dtac">
                                                <div class="">
                                                    <p class="p-internas tabfun3">Servicio</p>
                                                    <p class="text-internas text-justify">Clínica San Juan de Dios lo recibe con el profundo deseo de brindarle el mejor servicio en salud, y queremos que se sienta como en casa, rodeado de profesionales cuyo principal propósito es su bienestar y pronta recuperación.</p>
                                                    <p class="text-internas text-justify">Asimismo durante su estadía nos seguiremos esforzando para que depositen su confianza en nosotros, lo cual encierra una gran responsabilidad que debemos seguir profundizando a la luz los valores Juandedianos. </p>
                                                    <p class="text-internas text-justify">Para ello, estamos en un proceso de mayor acercamiento con ustedes, nuestros pacientes, mediante la implementación de acciones que den a conocer en forma más efectiva el valor diferencial de nuestra institución, nuestro Carisma Hospitalario y nuestra misión, siendo conocedores que lo más valioso que tiene Clínica es el servicio y la calidad de atención.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.25s">
                                        <img src="assets/images/internas/inf-usuario/ser1.jpg" alt="" class="w-100 pl3 noPadImg-sm">
                                    </div>
                                </div>

                                <!-- ADMISIÓN POR INTERNAMIENTO -->
                                <div class="col-xs-12 pb7">
                                    <div class="row">
                                        <div class="posrel flex-reverseMovil">
                                            <img src="assets/images/internas/inf-usuario/ser2.jpg" alt="" class="w-100 pr3 noPadImg-sm">
                                            <div class="btabr">
                                                <p class="p-internas tabfun3">Admisión<br>por internamiento</p>
                                                <p class="text-internas text-justify">El día de su internamiento deberá ingresar por las oficinas de Admisión Hospitalaria, en el horario que se le haya indicado por parte de Planificación Hospitalaria.</p>
                                                <p class="text-internas text-justify azul bold sinb">Se le solicitará:</p>
                                                <p class="text-internas text-justify">Documento de Identidad y copia de la carta de garantía</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- TOMAR EN CUENTA -->
                                <div class="row dflex nflex pb7">
                                    <div class="col-xs-12 col-md-5">
                                        <div class="dta">
                                            <div class="dtac">
                                                <div class="">
                                                    <p class="p-internas tabfun3">Tomar en cuenta</p>
                                                    <ul>
                                                        <li class="text-internas sinb fleli text-justify">Estar acompañado de un familiar que si ante una intervención quirúrgica requiera permanecer con usted en la habitación.</li>
                                                        <li class="text-internas sinb fleli text-justify">Es necesario traer un bolso del cual deberá tener: pijama, artículo de aseo personal, toalla, pantuflas y sandalias.</li>
                                                        <li class="text-internas sinb fleli text-justify">Si su hospitalización consta de una intervención quirúrgica se solicita dejar la puerta de la habitación cerrada. </li>
                                                        <li class="text-internas sinb fleli text-justify">Es importante que tenga en cuenta que durante su hospitalización evite traer objetos personales, la Clínica no se hace responsable por objetos perdidos.</li>
                                                        <li class="text-internas sinb fleli text-justify">Las visitas sólo podrán ingresar con el carnet de visitante del cual será entregado por el área de seguridad que se encontrará ubicado en el primer piso.</li>
                                                        <li class="text-internas sinb fleli text-justify">Recuerde que la alimentación sólo es dada por la clínica y no se podrá traer alimentos fuera de lo indicado por el médico tratante.</li>
                                                    </ul>                           
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-7 ">
                                        <img src="assets/images/internas/inf-usuario/ser3.jpg" alt="" class="w-100 pl3 noPadImg-sm">
                                    </div>
                                </div>

                                <!-- DURANTE SU PERMANENCIA -->
                                <div class="row dflex flex-reverseMovil pb7">
                                    <div class="col-xs-12 col-md-7 ">
                                        <img src="assets/images/internas/inf-usuario/ser4.jpg" alt="" class="w-100 pr3 noPadImg-sm">
                                    </div>

                                    <div class="col-xs-12 col-md-5">
                                        <div class="dta">
                                            <div class="dtac">
                                                <div class="">
                                                    <p class="p-internas tabfun3">Durante su permanencia</p>
                                                    <ul>
                                                        <li class="text-internas sinb fleli text-justify">El paciente debe de permanecer acompañado por un familiar durante las 24 horas, evitar desplazarse solo por fuera de su habitación, si en caso el médico indique caminar fuera de la misma, hacerlo en compañía del familiar o persona asistencial. </li>
                                                        <li class="text-internas sinb fleli text-justify">Si en caso el paciente se pueda encontrar sólo es importante informar al personal de enfermería para el cuidado del paciente.</li>
                                                        <li class="text-internas sinb fleli text-justify">Si el paciente o su acompañante identifican en su atención algún acto inseguro que genere riesgo durante su hospitalización, infórmelo al personal asistencial o médico de piso.</li>
                                                        <li class="text-internas sinb fleli text-justify">Las camas son sólo para el uso exclusivo de los pacientes. Solicitamos a los acompañantes no sentarse o acostarse en ella.</li>
                                                        <li class="text-internas sinb fleli text-justify">Si en caso requiera de apoyo asistencial, por favor no dude en apretar el botón de llamado de enarenaría para que acuda a ayudarlo.</li>
                                                    </ul>                           
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- HORARIOS -->
                                <div class="row gdh-horarios">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="p-internas tabfun4 title-horarios">
                                            <span class="icon-clock"></span>
                                            <span class="title-horarios_text">Horarios</span>
                                        </p>

                                        <div class="espec row">
                                            <div class="col-md-4">
                                                <h3 class="specialty">Lunes a Viernes:</h3>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="p-internas spanSpecialty">8:00 am - 14:00 pm</span>
                                            </div>
                                        </div>
                                        
                                        <div class="espec row">
                                            <div class="col-md-4">
                                                <h3 class="specialty">Sábados:</h3>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="p-internas spanSpecialty">8:00 am - 13:00 pm</span>
                                            </div>
                                        </div>
                                        <button class="btn-primary btn btn-bus2">Agendar</button>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <div class="bta tet2 w-100 info-horarios">
                                            <p class="text-internas text-justify sinb">Una vez comunicado el ALTA MÉDICA por parte de su médico tratante, realizará el proceso de documentación y dejará en la estación de enfermeras las indicaciones médicas y los cuidados que debe tener en casa. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-inf-usuario.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>