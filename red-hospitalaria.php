<?php
    include 'src/includes/header.php'
?>
<main>
    <!--BANNER-->
    <section id="parallax" class="sct-banner scroll">
        <img class="img-banner" src="/assets/images/banner/redhospitalaria.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">Red hospitalaria</h1>
        </div>
    </section>
    <section class="sct-red-hospitalaria bg-white">
        <div class="container-fluid pd-x-0">
            <div class="row">
                <div class="tab-content">
                    <!-- PRESENCIA EN PERÚ -->
                    <div class="tab-pane active" id="tab1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent=".tab-pane" href="#collapseOne" class="color-primary titles-big">
                                PRESENCIA EN PERÚ
                                </a>
                            </h4>
                            </div>
                            
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="container-fluid pd-x-0">
                                        <div class="row">
                                            <div class="col-xs-12 col-lg-6 pd-x-0">
                                                <div class="container">
                                                    <div class="row">
                                                        
                                                        <div class="col-xs-12 pd-x-0 visible-lg">
                                                            <h2 class="titles-descrip">
                                                                <span class="icon-san"></span>
                                                                <span class="span-titlesDescrip">
                                                                    Presencia</span><br>en perú</h2>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <h2 class="sub-ttl-flotant color-primary">Red de Salud</h2>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                                <img class="img-cover img-color" src="assets/images/logos/clinica-lima.jpg" alt="">
                                                                                <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-lima.jpg" alt="">
                                                                            </a>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/clinica-piura.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-piura.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/clinica-aqp.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-aqp.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/compl-san-piura.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/compl-san-piura.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/clinica-cusco.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-cusco.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/clinica-iqt.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-iqt.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/clinica-chiclayo.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-chiclayo.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-xs-12 col-md-6">
                                                                <h2 class="sub-ttl-flotant color-primary">Docencia</h2>
                                                                <div class="col-xs-10 col-sm-12">
                                                                    <div class="img-clinicas">
                                                                        <a href="#">
                                                                        <img class="img-cover img-color" src="assets/images/logos/col-esp-aqp.jpg" alt="">
                                                                        <img class="img-cover img-opacity" src="assets/images/logos/black/col-esp-aqp.jpg" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-md-6">
                                                                <h2 class="sub-ttl-flotant color-primary">Investigación</h2>
                                                                <div class="col-xs-10 col-sm-12">
                                                                    <div class="img-clinicas">
                                                                        <a href="#">
                                                                        <img class="img-cover img-color" src="assets/images/logos/clinica-aqp.jpg" alt="">
                                                                        <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-aqp.jpg" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <h2 class="sub-ttl-flotant color-primary">Fundación</h2>
                                                                <div class="col-xs-10 col-sm-6">
                                                                    <div class="img-clinicas">
                                                                        <a href="#">
                                                                        <img class="img-cover img-color" src="assets/images/logos/fundac-telet.jpg" alt="">
                                                                        <img class="img-cover img-opacity" src="assets/images/logos/black/fundac-telet.jpg" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <h2 class="sub-ttl-flotant color-primary">Otros servicios</h2>
                                                                <div class="col-xs-10 col-sm-6">
                                                                    <div class="img-clinicas">
                                                                        <a href="#">
                                                                        <img class="img-cover img-color" src="assets/images/logos/hotel-cusco.jpg" alt="">
                                                                        <img class="img-cover img-opacity" src="assets/images/logos/black/hotel-cusco.jpg" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-10 col-sm-6">
                                                                    <div class="img-clinicas">
                                                                        <a href="#">
                                                                        <img class="img-cover img-color" src="assets/images/logos/asoc-de-coop-peru.jpg" alt="">
                                                                        <img class="img-cover img-opacity" src="assets/images/logos/black/asoc-de-coop-peru.jpg" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 pd-x-0 visible-lg">
                                                <div class="bg-transp"></div>
                                                <div class="img-tabs-country">
                                                    <img class="img-cover" src="assets/images/paises/peru.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PRESENCIA EN ECUADOR -->
                    <div class="tab-pane" id="tab2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent=".tab-pane" href="#collapseTwo" class="color-primary titles-big">
                                PRESENCIA EN ECUADOR
                                </a>
                            </h4>
                            </div>
                            
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="container-fluid pd-x-0">
                                        <div class="row">
                                            <div class="col-xs-12 col-lg-6 pd-x-0">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-12 pd-x-0 visible-lg">
                                                            <h2 class="titles-descrip">
                                                                <span class="icon-san"></span>
                                                                <span class="span-titlesDescrip">
                                                                    Presencia</span><br>en Ecuador</h2>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <h2 class="sub-ttl-flotant color-primary">Red de Salud</h2>
                                                                    <div class="col-xs-10 col-sm-6">
                                                                        <div class="img-clinicas">
                                                                            <a href="#">
                                                                            <img class="img-cover img-color" src="assets/images/logos/hospital-esp-ecuador.jpg" alt="">
                                                                            <img class="img-cover img-opacity" src="assets/images/logos/black/hospital-esp-ecuador.jpg" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-xs-12">
                                                                <h2 class="sub-ttl-flotant color-primary">Social</h2>
                                                                <div class="col-xs-10 col-sm-6">
                                                                    <div class="img-clinicas">
                                                                        <a href="#">
                                                                        <img class="img-cover img-color" src="assets/images/logos/albergue-ecuador.jpg" alt="">
                                                                        <img class="img-cover img-opacity" src="assets/images/logos/black/albergue-ecuador.jpg" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 pd-x-0 visible-lg">
                                                <div class="bg-transp"></div>
                                                <div class="img-tabs-country">
                                                    <img class="img-cover" src="assets/images/paises/ecuador.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PRESENCIA EN VENEZUELA -->
                    <div class="tab-pane" id="tab3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent=".tab-pane" href="#collapseThree" class="color-primary titles-big">
                                PRESENCIA EN VENEZUELA
                                </a>
                            </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-xs-12 col-lg-6 pd-x-0">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 pd-x-0 visible-lg">
                                                    <h2 class="titles-descrip">
                                                        <span class="icon-san"></span>
                                                        <span class="span-titlesDescrip">
                                                            Presencia</span><br>en Venezuela</h2>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h2 class="sub-ttl-flotant color-primary">Red de Salud</h2>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="img-clinicas">
                                                                    <a href="#">
                                                                    <img class="img-cover img-color" src="assets/images/logos/clinica-san-rafael-venezuela.jpg" alt="">
                                                                    <img class="img-cover img-opacity" src="assets/images/logos/black/clinica-san-rafael-venezuela.jpg" alt="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-10 col-sm-6">
                                                                <div class="img-clinicas">
                                                                    <a href="#">
                                                                    <img class="img-cover img-color" src="assets/images/logos/hospital-caracas.jpg" alt="">
                                                                    <img class="img-cover img-opacity" src="assets/images/logos/black/hospital-caracas.jpg" alt="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-xs-12">
                                                        <h2 class="sub-ttl-flotant color-primary">Fundación</h2>
                                                        <div class="col-xs-10 col-sm-6">
                                                            <div class="img-clinicas">
                                                                <a href="#">
                                                                <img class="img-cover img-color" src="assets/images/logos/fundac-amigos-venezuela.jpg" alt="">
                                                                <img class="img-cover img-opacity" src="assets/images/logos/black/fundac-amigos-venezuela.jpg" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 pd-x-0 visible-lg">
                                        <div class="bg-transp"></div>
                                        <div class="img-tabs-country">
                                            <img class="img-cover" src="assets/images/paises/venezuela.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="nav nav-tabs col-xs-6 pd-x-0">
                    <li class="active isotipo-tab">
                        <a href="#tab1" data-toggle="tab">
                            <div class="icon-ico">
                                <div class="pulse"></div>
                            </div>
                        </a>
                    </li>  
                    <li class="isotipo-tab">
                        <a href="#tab2" data-toggle="tab">
                            <div class="icon-ico">
                                <div class="pulse"></div>
                            </div>
                        </a>
                    </li>
                    <li class="isotipo-tab">
                        <a href="#tab3" data-toggle="tab">
                            <div class="icon-ico">
                                <div class="pulse"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <div class="div col-xs-5 map-tab visible-lg">
                    <div>
                        <img class="img-cover" src="assets/images/mapa.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</main>
<?php
    include 'src/includes/footer.php'
?>
</body>

</html>